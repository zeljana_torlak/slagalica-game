package helper;

import java.io.Serializable;
import java.util.LinkedList;

public class AKlasa implements Serializable {

    public String[][] kolone;
    public String[] resenjaKolona;
    public LinkedList<String> konacno;

    public AKlasa() {
        kolone = new String[4][];
        for (int i = 0; i < 4; i++) {
            kolone[i] = new String[4];
        }
        resenjaKolona = new String[4];
        konacno = new LinkedList<>();
    }

    public String[][] getKolone() {
        return kolone;
    }

    public void setKolone(String[][] kolone) {
        this.kolone = kolone;
    }

    public String[] getResenjaKolona() {
        return resenjaKolona;
    }

    public void setResenjaKolona(String[] resenjaKolona) {
        this.resenjaKolona = resenjaKolona;
    }

    public LinkedList<String> getKonacno() {
        return konacno;
    }

    public void setKonacno(LinkedList<String> konacno) {
        this.konacno = konacno;
    }

}
