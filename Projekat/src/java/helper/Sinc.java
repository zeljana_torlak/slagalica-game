package helper;

import java.io.Serializable;

public class Sinc implements Serializable {

    SLKlasa slagalica;
    int brojPoenaPlavogIgracaSlagalica;
    boolean poeniSlagalica;

    MBKlasa mojBroj;
    String tacanBroj;
    int brojPlavogIgraca;
    boolean poeniMojBroj;

    SKKlasa skocko;
    boolean sledeciIgracCrveniSkocko;
    String[][] simboli;
    boolean[] okSimbola;

    SPKlasa spojnice;
    boolean sledeciIgracCrveniSpojnice;
    String[] drugaKolona;
    boolean[][] okSpojnice;

    AKlasa asocijacije;
    boolean daLiJePrviIgracAsocijacije;
    boolean[][] okAsocijacija;
    String[][] bojeAsocijacija;
    String[][] otvorenaPoljaAsocijacija;
    boolean[] okResenjaAsocijacija;
    String[] bojeResenjaAsocijacija;
    String[] otvorenaResenjaAsocijacija;

    public Sinc() {
        //slagalica
        slagalica = new SLKlasa();
        for (int i = 0; i < 12; i++) {
            slagalica.slova[i] = "";
        }
        brojPoenaPlavogIgracaSlagalica = -1;
        poeniSlagalica = false;

        //mojBroj
        mojBroj = new MBKlasa();
        for (int i = 0; i < 6; i++) {
            mojBroj.brojevi[i] = -1;
        }
        tacanBroj = "???";
        brojPlavogIgraca = -1;
        poeniMojBroj = false;

        //skocko
        skocko = new SKKlasa();
        for (int i = 0; i < 4; i++) {
            skocko.kombinacija[i] = "?";
        }
        sledeciIgracCrveniSkocko = false;
        simboli = new String[7][];
        okSimbola = new boolean[7];
        for (int i = 0; i < 7; i++) {
            simboli[i] = new String[4];
            okSimbola[i] = false;
        }

        //spojnice
        spojnice = null;
        sledeciIgracCrveniSpojnice = false;
        drugaKolona = null;
        okSpojnice = null;

        //asocijacije
        asocijacije = null;
        daLiJePrviIgracAsocijacije = true;
        okAsocijacija = null;
        bojeAsocijacija = null;
        otvorenaPoljaAsocijacija = null;
        okResenjaAsocijacija = null;
        bojeResenjaAsocijacija = null;
        otvorenaResenjaAsocijacija = null;
    }

    public SLKlasa getSlagalica() {
        return slagalica;
    }

    public void setSlagalica(SLKlasa slagalica) {
        this.slagalica = slagalica;
    }

    public int getBrojPoenaPlavogIgracaSlagalica() {
        return brojPoenaPlavogIgracaSlagalica;
    }

    public void setBrojPoenaPlavogIgracaSlagalica(int brojPoenaPlavogIgracaSlagalica) {
        this.brojPoenaPlavogIgracaSlagalica = brojPoenaPlavogIgracaSlagalica;
    }

    public boolean isPoeniSlagalica() {
        return poeniSlagalica;
    }

    public void setPoeniSlagalica(boolean poeniSlagalica) {
        this.poeniSlagalica = poeniSlagalica;
    }

    public MBKlasa getMojBroj() {
        return mojBroj;
    }

    public void setMojBroj(MBKlasa mojBroj) {
        this.mojBroj = mojBroj;
    }

    public String getTacanBroj() {
        return tacanBroj;
    }

    public void setTacanBroj(String tacanBroj) {
        this.tacanBroj = tacanBroj;
    }

    public int getBrojPlavogIgraca() {
        return brojPlavogIgraca;
    }

    public void setBrojPlavogIgraca(int brojPlavogIgraca) {
        this.brojPlavogIgraca = brojPlavogIgraca;
    }

    public boolean isPoeniMojBroj() {
        return poeniMojBroj;
    }

    public void setPoeniMojBroj(boolean poeniMojBroj) {
        this.poeniMojBroj = poeniMojBroj;
    }

    public SKKlasa getSkocko() {
        return skocko;
    }

    public void setSkocko(SKKlasa skocko) {
        this.skocko = skocko;
    }

    public boolean isSledeciIgracCrveniSkocko() {
        return sledeciIgracCrveniSkocko;
    }

    public void setSledeciIgracCrveniSkocko(boolean sledeciIgracCrveniSkocko) {
        this.sledeciIgracCrveniSkocko = sledeciIgracCrveniSkocko;
    }

    public String[][] getSimboli() {
        return simboli;
    }

    public void setSimboli(String[][] simboli) {
        this.simboli = simboli;
    }

    public boolean[] getOkSimbola() {
        return okSimbola;
    }

    public void setOkSimbola(boolean[] okSimbola) {
        this.okSimbola = okSimbola;
    }

    public SPKlasa getSpojnice() {
        return spojnice;
    }

    public void setSpojnice(SPKlasa spojnice) {
        this.spojnice = spojnice;
    }

    public boolean isSledeciIgracCrveniSpojnice() {
        return sledeciIgracCrveniSpojnice;
    }

    public void setSledeciIgracCrveniSpojnice(boolean sledeciIgracCrveniSpojnice) {
        this.sledeciIgracCrveniSpojnice = sledeciIgracCrveniSpojnice;
    }

    public String[] getDrugaKolona() {
        return drugaKolona;
    }

    public void setDrugaKolona(String[] drugaKolona) {
        this.drugaKolona = drugaKolona;
    }

    public boolean[][] getOkSpojnice() {
        return okSpojnice;
    }

    public void setOkSpojnice(boolean[][] okSpojnice) {
        this.okSpojnice = okSpojnice;
    }

    public AKlasa getAsocijacije() {
        return asocijacije;
    }

    public void setAsocijacije(AKlasa asocijacije) {
        this.asocijacije = asocijacije;
    }

    public boolean isDaLiJePrviIgracAsocijacije() {
        return daLiJePrviIgracAsocijacije;
    }

    public void setDaLiJePrviIgracAsocijacije(boolean daLiJePrviIgracAsocijacije) {
        this.daLiJePrviIgracAsocijacije = daLiJePrviIgracAsocijacije;
    }

    public boolean[][] getOkAsocijacija() {
        return okAsocijacija;
    }

    public void setOkAsocijacija(boolean[][] okAsocijacija) {
        this.okAsocijacija = okAsocijacija;
    }

    public String[] getBojeResenjaAsocijacija() {
        return bojeResenjaAsocijacija;
    }

    public void setBojeResenjaAsocijacija(String[] bojeResenjaAsocijacija) {
        this.bojeResenjaAsocijacija = bojeResenjaAsocijacija;
    }

    public String[][] getBojeAsocijacija() {
        return bojeAsocijacija;
    }

    public void setBojeAsocijacija(String[][] bojeAsocijacija) {
        this.bojeAsocijacija = bojeAsocijacija;
    }

    public String[][] getOtvorenaPoljaAsocijacija() {
        return otvorenaPoljaAsocijacija;
    }

    public void setOtvorenaPoljaAsocijacija(String[][] otvorenaPoljaAsocijacija) {
        this.otvorenaPoljaAsocijacija = otvorenaPoljaAsocijacija;
    }

    public boolean[] getOkResenjaAsocijacija() {
        return okResenjaAsocijacija;
    }

    public void setOkResenjaAsocijacija(boolean[] okResenjaAsocijacija) {
        this.okResenjaAsocijacija = okResenjaAsocijacija;
    }

    public String[] getOtvorenaResenjaAsocijacija() {
        return otvorenaResenjaAsocijacija;
    }

    public void setOtvorenaResenjaAsocijacija(String[] otvorenaResenjaAsocijacija) {
        this.otvorenaResenjaAsocijacija = otvorenaResenjaAsocijacija;
    }

}
