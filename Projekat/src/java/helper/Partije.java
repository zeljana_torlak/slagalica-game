package helper;

import java.io.Serializable;

public class Partije implements Serializable {

    int plaviSlagalica;
    int crveniSlagalica;

    int plaviMojBroj;
    int crveniMojBroj;

    int plaviSkocko;
    int crveniSkocko;

    int plaviSpojnice;
    int crveniSpojnice;

    int plaviAsocijacije;
    int crveniAsocijacije;

    public int getPlaviSlagalica() {
        return plaviSlagalica;
    }

    public void setPlaviSlagalica(int plaviSlagalica) {
        this.plaviSlagalica = plaviSlagalica;
    }

    public int getCrveniSlagalica() {
        return crveniSlagalica;
    }

    public void setCrveniSlagalica(int crveniSlagalica) {
        this.crveniSlagalica = crveniSlagalica;
    }

    public int getPlaviMojBroj() {
        return plaviMojBroj;
    }

    public void setPlaviMojBroj(int plaviMojBroj) {
        this.plaviMojBroj = plaviMojBroj;
    }

    public int getCrveniMojBroj() {
        return crveniMojBroj;
    }

    public void setCrveniMojBroj(int crveniMojBroj) {
        this.crveniMojBroj = crveniMojBroj;
    }

    public int getPlaviSkocko() {
        return plaviSkocko;
    }

    public void setPlaviSkocko(int plaviSkocko) {
        this.plaviSkocko = plaviSkocko;
    }

    public int getCrveniSkocko() {
        return crveniSkocko;
    }

    public void setCrveniSkocko(int crveniSkocko) {
        this.crveniSkocko = crveniSkocko;
    }

    public int getPlaviSpojnice() {
        return plaviSpojnice;
    }

    public void setPlaviSpojnice(int plaviSpojnice) {
        this.plaviSpojnice = plaviSpojnice;
    }

    public int getCrveniSpojnice() {
        return crveniSpojnice;
    }

    public void setCrveniSpojnice(int crveniSpojnice) {
        this.crveniSpojnice = crveniSpojnice;
    }

    public int getPlaviAsocijacije() {
        return plaviAsocijacije;
    }

    public void setPlaviAsocijacije(int plaviAsocijacije) {
        this.plaviAsocijacije = plaviAsocijacije;
    }

    public int getCrveniAsocijacije() {
        return crveniAsocijacije;
    }

    public void setCrveniAsocijacije(int crveniAsocijacije) {
        this.crveniAsocijacije = crveniAsocijacije;
    }

}
