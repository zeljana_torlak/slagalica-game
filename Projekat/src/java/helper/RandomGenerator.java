package helper;

import java.util.Random;

public class RandomGenerator {

    Random rand = new Random();

    private String[] slova = {"A", "B", "V", "G", "D", "Đ", "E", "Ž", "Z", "I",
        "J", "K", "L", "Lj", "M", "N", "Nj", "O", "P", "R",
        "S", "T", "Ć", "U", "F", "H", "C", "Č", "Dž", "Š"};

    public String[] getSlova() {
        return slova;
    }

    public String randomSlovo() { //bazirano na verovatnocama pojavljivanja slova u srpskom jeziku 
        double r = rand.nextDouble();
        if (r < 0.1240) {
            return slova[0];
        } else if (r < 0.1396) {
            return slova[1];
        } else if (r < 0.1762) {
            return slova[2];
        } else if (r < 0.1943) {
            return slova[3];
        } else if (r < 0.2336) {
            return slova[4];
        } else if (r < 0.2359) {
            return slova[5];
        } else if (r < 0.3272) {
            return slova[6];
        } else if (r < 0.3328) {
            return slova[7];
        } else if (r < 0.3505) {
            return slova[8];
        } else if (r < 0.4440) {
            return slova[9];
        } else if (r < 0.4731) {
            return slova[10];
        } else if (r < 0.5078) {
            return slova[11];
        } else if (r < 0.5372) {
            return slova[12];
        } else if (r < 0.5420) {
            return slova[13];
        } else if (r < 0.5782) {
            return slova[14];
        } else if (r < 0.6333) {
            return slova[15];
        } else if (r < 0.6409) {
            return slova[16];
        } else if (r < 0.7405) {
            return slova[17];
        } else if (r < 0.7697) {
            return slova[18];
        } else if (r < 0.8179) {
            return slova[19];
        } else if (r < 0.8681) {
            return slova[20];
        } else if (r < 0.9128) {
            return slova[21];
        } else if (r < 0.9189) {
            return slova[22];
        } else if (r < 0.9610) {
            return slova[23];
        } else if (r < 0.9630) {
            return slova[24];
        } else if (r < 0.9691) {
            return slova[25];
        } else if (r < 0.9760) {
            return slova[26];
        } else if (r < 0.9875) {
            return slova[27];
        } else if (r < 0.9878) {
            return slova[28];
        } else {
            return slova[29];
        }
    }

    private String[] simboli = {"herc", "tref", "karo",
        "pik", "zvezda", "skocko"};

    public String[] getSimboli() {
        return simboli;
    }

    public String randomSimbol() {
        int r = rand.nextInt(6);
        return simboli[r];
    }

    public int randomBroj() {
        return rand.nextInt(10);
    }

    public int randomMaliBroj() {
        return 1 + rand.nextInt(9);
    }

    public int randomSrednjiBroj() {
        return (2 + rand.nextInt(3)) * 5;
    }

    public int randomVelikiBroj() {
        return (1 + rand.nextInt(4)) * 25;
    }
}
