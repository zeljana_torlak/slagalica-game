package helper;

import java.io.Serializable;

public class SPKlasa implements Serializable {

    public String[] kolona1;
    public String[] kolona2;

    public SPKlasa() {
        kolona1 = new String[10];
        kolona2 = new String[10];
    }

    public String[] getKolona1() {
        return kolona1;
    }

    public void setKolona1(String[] kolona1) {
        this.kolona1 = kolona1;
    }

    public String[] getKolona2() {
        return kolona2;
    }

    public void setKolona2(String[] kolona2) {
        this.kolona2 = kolona2;
    }

}
