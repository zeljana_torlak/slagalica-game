package helper;

import java.io.Serializable;

public class MBKlasa implements Serializable {

    public int mojBroj;
    public int[] brojevi;

    public MBKlasa() {
        brojevi = new int[6];
    }

    public int getMojBroj() {
        return mojBroj;
    }

    public void setMojBroj(int mojBroj) {
        this.mojBroj = mojBroj;
    }

    public int[] getBrojevi() {
        return brojevi;
    }

    public void setBrojevi(int[] brojevi) {
        this.brojevi = brojevi;
    }

}
