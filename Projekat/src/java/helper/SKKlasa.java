package helper;

import java.io.Serializable;

public class SKKlasa implements Serializable {

    public String[] kombinacija;

    public SKKlasa() {
        kombinacija = new String[4];
    }

    public String[] getKombinacija() {
        return kombinacija;
    }

    public void setKombinacija(String[] kombinacija) {
        this.kombinacija = kombinacija;
    }

}
