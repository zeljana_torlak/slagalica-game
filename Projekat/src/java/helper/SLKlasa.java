package helper;

import java.io.Serializable;

public class SLKlasa implements Serializable {

    public String[] slova;

    public SLKlasa() {
        slova = new String[12];
    }

    public String[] getSlova() {
        return slova;
    }

    public void setSlova(String[] slova) {
        this.slova = slova;
    }

}
