package kontroleri.partija;

import db.HibernateUtil;
import entiteti.Igra;
import entiteti.Korisnik;
import entiteti.Sinhronizacija;
import helper.Partije;
import helper.RandomGenerator;
import helper.SKKlasa;
import helper.Sinc;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Skocko2 {

    public String[] naziviSimbola = {"skocko", "herc", "pik", "karo", "tref", "zvezda"};

    String korIme;
    Korisnik kor;
    int igraID;
    String bojaIgraca;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;
    int ukupanBrojPoena;

    RandomGenerator rand;
    String[][] simboli;
    int sledeciRed;
    int brojSimbolaURedu;
    boolean pokrenut, kraj;
    String[] pogodak;

    boolean sledeci;
    String[] tacnaKombinacija;
    boolean daLiPostojiJosJednaIgra;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public String[][] getSimboli() {
        return simboli;
    }

    public void setSimboli(String[][] simboli) {
        this.simboli = simboli;
    }

    public int getSledeciRed() {
        return sledeciRed;
    }

    public void setSledeciRed(int sledeciRed) {
        this.sledeciRed = sledeciRed;
    }

    public int getBrojSimbolaURedu() {
        return brojSimbolaURedu;
    }

    public void setBrojSimbolaURedu(int brojSimbolaURedu) {
        this.brojSimbolaURedu = brojSimbolaURedu;
    }

    public boolean isPokrenut() {
        return pokrenut;
    }

    public void setPokrenut(boolean pokrenut) {
        this.pokrenut = pokrenut;
    }

    public String[] getPogodak() {
        return pogodak;
    }

    public void setPogodak(String[] pogodak) {
        this.pogodak = pogodak;
    }

    public String[] getNaziviSimbola() {
        return naziviSimbola;
    }

    public void setNaziviSimbola(String[] naziviSimbola) {
        this.naziviSimbola = naziviSimbola;
    }

    public boolean isKraj() {
        return kraj;
    }

    public void setKraj(boolean kraj) {
        this.kraj = kraj;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public String getBojaIgraca() {
        return bojaIgraca;
    }

    public void setBojaIgraca(String bojaIgraca) {
        this.bojaIgraca = bojaIgraca;
    }

    public int getUkupanBrojPoena() {
        return ukupanBrojPoena;
    }

    public void setUkupanBrojPoena(int ukupanBrojPoena) {
        this.ukupanBrojPoena = ukupanBrojPoena;
    }

    public String[] getTacnaKombinacija() {
        return tacnaKombinacija;
    }

    public void setTacnaKombinacija(String[] tacnaKombinacija) {
        this.tacnaKombinacija = tacnaKombinacija;
    }

    public boolean isSledeci() {
        return sledeci;
    }

    public void setSledeci(boolean sledeci) {
        this.sledeci = sledeci;
    }

    public Skocko2() {
        simboli = new String[7][];
        for (int i = 0; i < 7; i++) {
            simboli[i] = new String[4];
        }
        preostaloVreme = 60;
        sledeciRed = poeni = 0;
        pokrenut = kraj = false;
        pogodak = new String[7];
        bodovanje = false;
        rand = new RandomGenerator();
        tacnaKombinacija = new String[4];
        sledeci = false;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        igraID = (int) hs.getAttribute("igraID");
        bojaIgraca = (String) hs.getAttribute("boja");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 2) {
            sledecaIgra();
        } else {
            stanje++;
        }

        Igra i = dohvatiIgru();
        if (daLiJeCrveniIgrac()) {
            ukupanBrojPoena = i.getPoeniCrvenog();
        }
        if (daLiJePlaviIgrac()) {
            ukupanBrojPoena = i.getPoeniPlavog();
        }

        Sinc sinc = dohvatiSinc();
        daLiPostojiJosJednaIgra = !sinc.isSledeciIgracCrveniSkocko();
        //FacesContext.getCurrentInstance().responseComplete();
    }

    public void sledecaIgra() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        hs.setAttribute("stanje", stanje);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("spojnice.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Skocko2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void zavrsiSkocka() {
        if (!bodovanje) {
            boduj();
        }
        sledecaIgra();
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        sacuvajZavrsenuIgru();
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("rezultati.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Skocko2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void kreni() {
        if (daLiJePrviIgrac()) {
            for (int i = 0; i < 4; i++) {
                tacnaKombinacija[i] = rand.randomSimbol();
            }

            Sinc sinc = dohvatiSinc();

            if (daLiJeCrveniIgrac()) {
                String[][] simboliSinca = new String[7][];
                boolean[] okSimbola = new boolean[7];
                for (int i = 0; i < 7; i++) {
                    simboliSinca[i] = new String[4];
                    okSimbola[i] = false;
                }

                sinc.setSimboli(simboliSinca);
                sinc.setOkSimbola(okSimbola);
            }

            SKKlasa skocko = sinc.getSkocko();
            skocko.setKombinacija(tacnaKombinacija);
            sinc.setSkocko(skocko);
            azurirajSinc(sinc);
        } else {
            Sinc sinc = dohvatiSinc();
            SKKlasa skocko = sinc.getSkocko();
            while (skocko.kombinacija[0].equals("?")) {
                sinc = dohvatiSinc();
                skocko = sinc.getSkocko();
            }

            for (int i = 0; i < 4; i++) {
                tacnaKombinacija[i] = skocko.kombinacija[i];
            }
        }

        pokrenut = true;
    }

    public void dodaj(int indexSimbola) {
        if (brojSimbolaURedu < 4) {
            simboli[sledeciRed][brojSimbolaURedu] = naziviSimbola[indexSimbola];
            brojSimbolaURedu++;
        }
    }

    public void brisi() {
        simboli[sledeciRed][brojSimbolaURedu - 1] = "";
        if (brojSimbolaURedu > 0) {
            brojSimbolaURedu--;
        }
    }

    public void proveri() {
        if (sadIgram()) {
            LinkedList<Integer> lista1 = new LinkedList<>();
            LinkedList<Integer> lista2 = new LinkedList<>();
            int crvena = 0;
            int zuta = 0;

            for (int i = 0; i < 4; i++) {
                if (simboli[sledeciRed][i].equals(tacnaKombinacija[i])) {
                    lista1.add(i);
                    lista2.add(i);
                    crvena++;
                }
            }

            for (int i = 0; i < 4; i++) {
                if (!lista1.contains(i)) {
                    for (int j = 0; j < 4; j++) {
                        if (!lista2.contains(j) && simboli[sledeciRed][i].equals(tacnaKombinacija[j])) {
                            lista2.add(j);
                            zuta++;
                            break;
                        }
                    }
                }
            }

            sledeciRed++;
            brojSimbolaURedu = 0;
            pogodak[sledeciRed - 1] = "slika" + crvena + "" + zuta;

            Sinc s = dohvatiSinc();
            s.setSimboli(simboli);
            boolean[] okSimb = s.getOkSimbola();
            okSimb[sledeciRed - 1] = true;
            s.setOkSimbola(okSimb);
            azurirajSinc(s);

            if (daLiJeSveURedu()) {
                if (!bodovanje) {
                    boduj();
                }
                kraj = true;
                sledeci = true;
            } else if (sledeciRed == 6) {
                sledeci = true;
            }
        } else {
            Sinc s = dohvatiSinc();
            while (!s.getOkSimbola()[sledeciRed]) {
                s = dohvatiSinc();
            }
            simboli[sledeciRed] = s.getSimboli()[sledeciRed];

            LinkedList<Integer> lista1 = new LinkedList<>();
            LinkedList<Integer> lista2 = new LinkedList<>();
            int crvena = 0;
            int zuta = 0;

            for (int i = 0; i < 4; i++) {
                if (simboli[sledeciRed][i].equals(tacnaKombinacija[i])) {
                    lista1.add(i);
                    lista2.add(i);
                    crvena++;
                }
            }

            for (int i = 0; i < 4; i++) {
                if (!lista1.contains(i)) {
                    for (int j = 0; j < 4; j++) {
                        if (!lista2.contains(j) && simboli[sledeciRed][i].equals(tacnaKombinacija[j])) {
                            lista2.add(j);
                            zuta++;
                            break;
                        }
                    }
                }
            }

            sledeciRed++;
            brojSimbolaURedu = 0;
            pogodak[sledeciRed - 1] = "slika" + crvena + "" + zuta;

            if (daLiJeSveURedu()) {
                if (!bodovanje) {
                    boduj();
                }
                kraj = true;
                sledeci = true;
            } else if (sledeciRed == 6) {
                sledeci = true;
            }
        }
    }

    public void smanjiVreme() {
        if (pokrenut) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void boduj() {
        bodovanje = true;
        if (daLiJeSveURedu()) {
            if (sadIgram()) {
                poeni = 10;
            }
        }

        azurirajPoene();

        Sinc s = dohvatiSinc();
        if (s.isSledeciIgracCrveniSkocko() == false) {
            s.setSledeciIgracCrveniSkocko(true);
            SKKlasa skocko = new SKKlasa();
            for (int i = 0; i < 4; i++) {
                skocko.kombinacija[i] = "?";
            }
            s.setSkocko(skocko);
            azurirajSinc(s);
        }
        if (daLiPostojiJosJednaIgra) {
            predjiNaSledecegIgraca();
        }
    }

    public boolean daLiJeSveURedu() {
        if (sledeciRed == 0) {
            return false;
        }
        for (int i = 0; i < 4; i++) {
            if (!simboli[sledeciRed - 1][i].equals(tacnaKombinacija[i])) {
                return false;
            }
        }
        return true;
    }

    public Sinc dohvatiSinc() {
        Sinhronizacija s;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Sinhronizacija.class);
        s = (Sinhronizacija) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return s.getSinc();
    }

    public void azurirajSinc(Sinc sinc) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Sinhronizacija s = (Sinhronizacija) session.get(Sinhronizacija.class, igraID);
        s.setSinc(sinc);
        session.update(s);

        session.getTransaction().commit();
        session.close();
    }

    public boolean daLiJePlaviIgrac() {
        return bojaIgraca.equals("plaviIgrac");
    }

    public boolean daLiJeCrveniIgrac() {
        return bojaIgraca.equals("crveniIgrac");
    }

    public Igra dohvatiIgru() {
        Igra i;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Igra.class);
        i = (Igra) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return i;
    }

    public void azurirajPoene() {
        if (daLiJePlaviIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setPlaviSkocko(partija.getPlaviSkocko() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniPlavog() + poeni;
            igra.setPoeniPlavog(igra.getPoeniPlavog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
        if (daLiJeCrveniIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setCrveniSkocko(partija.getCrveniSkocko() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniCrvenog() + poeni;
            igra.setPoeniCrvenog(igra.getPoeniCrvenog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
    }

    public void sacuvajZavrsenuIgru() {
        Igra i = dohvatiIgru();
        int poeniPlavog = i.getPoeniPlavog();
        int poeniCrvenog = i.getPoeniCrvenog();
        int ishod;
        if (poeniPlavog == poeniCrvenog) {
            ishod = 3;
        } else if (poeniPlavog > poeniCrvenog) {
            ishod = 1;
        } else {
            ishod = 2;
        }
        i.setIshod(ishod);
        i.setTip(3);

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        session.update(i);

        session.getTransaction().commit();
        session.close();
    }

    public void predjiNaSledecegIgraca() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        hs.setAttribute("stanje", stanje - 1);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("skocko.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Skocko2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public boolean daLiJePrviIgrac() {
        return (daLiJePlaviIgrac() && daLiPostojiJosJednaIgra) || (daLiJeCrveniIgrac() && !daLiPostojiJosJednaIgra);
    }

    public boolean sadIgram() {
        return (daLiJeCrveniIgrac() && !daLiJePrviIgrac() && sledeci == true) || (daLiJeCrveniIgrac() && daLiJePrviIgrac() && sledeci == false)
                || (daLiJePlaviIgrac() && !daLiJePrviIgrac() && sledeci == true) || (daLiJePlaviIgrac() && daLiJePrviIgrac() && sledeci == false);
    }
}
