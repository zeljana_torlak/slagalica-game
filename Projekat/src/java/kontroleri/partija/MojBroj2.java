package kontroleri.partija;

import db.HibernateUtil;
import entiteti.Igra;
import entiteti.Korisnik;
import entiteti.Sinhronizacija;
import helper.MBKlasa;
import helper.Partije;
import helper.RandomGenerator;
import helper.Sinc;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class MojBroj2 {

    String korIme;
    Korisnik kor;
    int igraID;
    String bojaIgraca;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;
    int ukupanBrojPoena;

    RandomGenerator rand;
    int sledeciBroj;
    boolean[] ok;
    String[] brojevi;
    int sledecaPozicija;
    int[] pozicije;
    String resenje;
    int rezultat;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public int getSledeciBroj() {
        return sledeciBroj;
    }

    public void setSledeciBroj(int sledeciBroj) {
        this.sledeciBroj = sledeciBroj;
    }

    public boolean[] getOk() {
        return ok;
    }

    public void setOk(boolean[] ok) {
        this.ok = ok;
    }

    public String[] getBrojevi() {
        return brojevi;
    }

    public void setBrojevi(String[] brojevi) {
        this.brojevi = brojevi;
    }

    public int getSledecaPozicija() {
        return sledecaPozicija;
    }

    public void setSledecaPozicija(int sledecaPozicija) {
        this.sledecaPozicija = sledecaPozicija;
    }

    public int[] getPozicije() {
        return pozicije;
    }

    public void setPozicije(int[] pozicije) {
        this.pozicije = pozicije;
    }

    public String getResenje() {
        return resenje;
    }

    public void setResenje(String resenje) {
        this.resenje = resenje;
    }

    public int getRezultat() {
        return rezultat;
    }

    public void setRezultat(int rezultat) {
        this.rezultat = rezultat;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public String getBojaIgraca() {
        return bojaIgraca;
    }

    public void setBojaIgraca(String bojaIgraca) {
        this.bojaIgraca = bojaIgraca;
    }

    public int getUkupanBrojPoena() {
        return ukupanBrojPoena;
    }

    public void setUkupanBrojPoena(int ukupanBrojPoena) {
        this.ukupanBrojPoena = ukupanBrojPoena;
    }

    public MojBroj2() {
        ok = new boolean[15];
        for (int i = 0; i < 15; i++) {
            ok[i] = true;
        }
        brojevi = new String[15];
        for (int i = 0; i < 9; i++) {
            brojevi[i] = "?";
        }
        brojevi[9] = "(";
        brojevi[10] = ")";
        brojevi[11] = "+";
        brojevi[12] = "-";
        brojevi[13] = "*";
        brojevi[14] = "/";
        preostaloVreme = 60;
        sledecaPozicija = poeni = rezultat = 0;
        pozicije = new int[30];
        resenje = "";
        bodovanje = false;
        rand = new RandomGenerator();

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        igraID = (int) hs.getAttribute("igraID");
        bojaIgraca = (String) hs.getAttribute("boja");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 1) {
            sledecaIgra();
        } else {
            stanje++;
        }

        Igra i = dohvatiIgru();
        if (daLiJeCrveniIgrac()) {
            ukupanBrojPoena = i.getPoeniCrvenog();
        }
        if (daLiJePlaviIgrac()) {
            ukupanBrojPoena = i.getPoeniPlavog();
        }
        //FacesContext.getCurrentInstance().responseComplete();
    }

    public void sledecaIgra() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        hs.setAttribute("stanje", stanje);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("skocko.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MojBroj2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void zavrsiMojBroj() {
        if (!bodovanje) {
            boduj();
        }
        sledecaIgra();
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        sacuvajZavrsenuIgru();
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("rezultati.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MojBroj2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void stopBroj() {
        if (daLiJePlaviIgrac()) {
            Sinc sinc = dohvatiSinc();
            MBKlasa mojBroj = sinc.getMojBroj();

            switch (sledeciBroj) {
                case 0:
                    brojevi[sledeciBroj] = rand.randomMaliBroj() + "";
                    sinc.setTacanBroj(brojevi[sledeciBroj] + "??");
                    break;
                case 1:
                    brojevi[sledeciBroj] = rand.randomBroj() + "";
                    sinc.setTacanBroj(sinc.getTacanBroj().substring(0, 1) + brojevi[sledeciBroj] + "?");
                    break;
                case 2:
                    brojevi[sledeciBroj] = rand.randomBroj() + "";
                    sinc.setTacanBroj(sinc.getTacanBroj().substring(0, 2) + brojevi[sledeciBroj]);
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    brojevi[sledeciBroj] = (mojBroj.brojevi[sledeciBroj - 3] = rand.randomMaliBroj()) + "";
                    break;
                case 7:
                    brojevi[sledeciBroj] = (mojBroj.brojevi[4] = rand.randomSrednjiBroj()) + "";
                    break;
                case 8:
                    brojevi[sledeciBroj] = (mojBroj.brojevi[5] = rand.randomVelikiBroj()) + "";
                    mojBroj.mojBroj = Integer.parseInt(brojevi[0] + brojevi[1] + brojevi[2]);
                    break;
                default:
                    break;
            }
            ok[sledeciBroj] = false;
            sledeciBroj++;

            if (sledeciBroj == 9) {
                ok[9] = ok[10] = ok[11] = ok[12] = ok[13] = ok[14] = false;
            }
            sinc.setMojBroj(mojBroj);
            azurirajSinc(sinc);
        }
        if (daLiJeCrveniIgrac()) {
            Sinc sinc = dohvatiSinc();

            if (sledeciBroj < 3) {
                while (sinc.getTacanBroj().substring(sledeciBroj, sledeciBroj + 1).equals("?")) {
                    sinc = dohvatiSinc();
                }
                brojevi[sledeciBroj] = sinc.getTacanBroj().substring(sledeciBroj, sledeciBroj + 1);
            } else {
                MBKlasa mojBroj = sinc.getMojBroj();
                while (mojBroj.brojevi[sledeciBroj - 3] == -1) {
                    sinc = dohvatiSinc();
                    mojBroj = sinc.getMojBroj();
                }
                brojevi[sledeciBroj] = mojBroj.brojevi[sledeciBroj - 3] + "";
            }
            ok[sledeciBroj] = false;
            sledeciBroj++;
            if (sledeciBroj == 9) {
                ok[9] = ok[10] = ok[11] = ok[12] = ok[13] = ok[14] = false;
            }
        }
    }

    public void dodaj(int indexBroj) {
        resenje = resenje + brojevi[indexBroj];
        pozicije[sledecaPozicija] = indexBroj;
        if (indexBroj < 9) {
            ok[indexBroj] = true;
        }
        sledecaPozicija++;
    }

    public void brisi() {
        sledecaPozicija--;
        int indexBroj = pozicije[sledecaPozicija];
        ok[indexBroj] = false;
        resenje = resenje.substring(0, resenje.length() - brojevi[indexBroj].length());
    }

    public void smanjiVreme() {
        if (sledeciBroj > 8) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void boduj() {
        bodovanje = true;
        if (!daLiJeSveURedu()) {
            rezultat = Integer.MAX_VALUE;
        }
        int temp = 0;
        try {
            temp = Integer.parseInt(brojevi[0] + brojevi[1] + brojevi[2]);
        } catch (NumberFormatException e) {
            rezultat = Integer.MAX_VALUE;
        }

        if (daLiJePlaviIgrac()) {
            Sinc sinc1 = dohvatiSinc();
            sinc1.setBrojPlavogIgraca(rezultat);
            azurirajSinc(sinc1);

            sinc1 = dohvatiSinc();
            while (sinc1.isPoeniMojBroj() == false) {
                sinc1 = dohvatiSinc();
            }

            poeni = sinc1.getBrojPlavogIgraca(); //brojPlavogIgraca u sincu se posle koristi za smestanje poena

            azurirajPoene();
        }
        if (daLiJeCrveniIgrac()) {
            Sinc sinc2 = dohvatiSinc();
            while (sinc2.getBrojPlavogIgraca() == -1) {
                sinc2 = dohvatiSinc();
            }
            if (rezultat == Integer.MAX_VALUE) {
                if (rezultat == sinc2.getBrojPlavogIgraca()) {
                    sinc2.setBrojPlavogIgraca(0);
                } else {
                    sinc2.setBrojPlavogIgraca(10);
                }
                poeni = 0;
            } else if (sinc2.getBrojPlavogIgraca() == Integer.MAX_VALUE) {
                sinc2.setBrojPlavogIgraca(0);
                poeni = 10;
            } else if (rezultat == sinc2.getBrojPlavogIgraca()) {
                sinc2.setBrojPlavogIgraca(5);
                poeni = 5;
            } else if (Math.abs(rezultat - temp) > Math.abs(sinc2.getBrojPlavogIgraca() - temp)) {
                sinc2.setBrojPlavogIgraca(10);
                poeni = 0;
            } else if (Math.abs(rezultat - temp) < Math.abs(sinc2.getBrojPlavogIgraca() - temp)) {
                sinc2.setBrojPlavogIgraca(0);
                poeni = 10;
            }

            sinc2.setPoeniMojBroj(true);
            azurirajSinc(sinc2);

            azurirajPoene();
        }
    }

    public boolean daLiJeSveURedu() {
        try {
            rezultat = (int) new ScriptEngineManager().getEngineByName("JavaScript").eval(resenje);
            return true;
        } catch (ScriptException ex) {
            rezultat = 1 / 0;
            Logger.getLogger(MojBroj2.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public Sinc dohvatiSinc() {
        Sinhronizacija s;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Sinhronizacija.class);
        s = (Sinhronizacija) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return s.getSinc();
    }

    public void azurirajSinc(Sinc sinc) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Sinhronizacija s = (Sinhronizacija) session.get(Sinhronizacija.class, igraID);
        s.setSinc(sinc);
        session.update(s);

        session.getTransaction().commit();
        session.close();
    }

    public boolean daLiJePlaviIgrac() {
        return bojaIgraca.equals("plaviIgrac");
    }

    public boolean daLiJeCrveniIgrac() {
        return bojaIgraca.equals("crveniIgrac");
    }

    public Igra dohvatiIgru() {
        Igra i;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Igra.class);
        i = (Igra) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return i;
    }

    public void azurirajPoene() {
        if (daLiJePlaviIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setPlaviMojBroj(partija.getPlaviMojBroj() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniPlavog() + poeni;
            igra.setPoeniPlavog(igra.getPoeniPlavog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
        if (daLiJeCrveniIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setCrveniMojBroj(partija.getCrveniMojBroj() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniCrvenog() + poeni;
            igra.setPoeniCrvenog(igra.getPoeniCrvenog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
    }

    public void sacuvajZavrsenuIgru() {
        Igra i = dohvatiIgru();
        int poeniPlavog = i.getPoeniPlavog();
        int poeniCrvenog = i.getPoeniCrvenog();
        int ishod;
        if (poeniPlavog == poeniCrvenog) {
            ishod = 3;
        } else if (poeniPlavog > poeniCrvenog) {
            ishod = 1;
        } else {
            ishod = 2;
        }
        i.setIshod(ishod);
        i.setTip(3);

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        session.update(i);

        session.getTransaction().commit();
        session.close();
    }

}
