package kontroleri.partija;

import db.HibernateUtil;
import entiteti.Igra;
import entiteti.Korisnik;
import entiteti.Sinhronizacija;
import helper.Partije;
import helper.SPKlasa;
import helper.Sinc;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Spojnice2 {

    String korIme;
    Korisnik kor;
    int igraID;
    String bojaIgraca;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;
    int ukupanBrojPoena;

    SPKlasa spojnice;
    String[] drugaKolona;
    int sledecaPozicija;
    boolean pokrenut;
    int brojPogodaka;
    boolean[] ok;
    String[][] boja;

    boolean sledeci;
    SPKlasa tacnaKombinacija;
    boolean daLiPostojiJosJednaIgra;
    boolean[][] okZaSlanje;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public String[] getDrugaKolona() {
        return drugaKolona;
    }

    public void setDrugaKolona(String[] drugaKolona) {
        this.drugaKolona = drugaKolona;
    }

    public int getSledecaPozicija() {
        return sledecaPozicija;
    }

    public void setSledecaPozicija(int sledecaPozicija) {
        this.sledecaPozicija = sledecaPozicija;
    }

    public boolean isPokrenut() {
        return pokrenut;
    }

    public void setPokrenut(boolean pokrenut) {
        this.pokrenut = pokrenut;
    }

    public int getBrojPogodaka() {
        return brojPogodaka;
    }

    public void setBrojPogodaka(int brojPogodaka) {
        this.brojPogodaka = brojPogodaka;
    }

    public SPKlasa getSpojnice() {
        return spojnice;
    }

    public void setSpojnice(SPKlasa spojnice) {
        this.spojnice = spojnice;
    }

    public boolean[] getOk() {
        return ok;
    }

    public void setOk(boolean[] ok) {
        this.ok = ok;
    }

    public String[][] getBoja() {
        return boja;
    }

    public void setBoja(String[][] boja) {
        this.boja = boja;
    }

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public String getBojaIgraca() {
        return bojaIgraca;
    }

    public void setBojaIgraca(String bojaIgraca) {
        this.bojaIgraca = bojaIgraca;
    }

    public int getUkupanBrojPoena() {
        return ukupanBrojPoena;
    }

    public void setUkupanBrojPoena(int ukupanBrojPoena) {
        this.ukupanBrojPoena = ukupanBrojPoena;
    }

    public boolean isSledeci() {
        return sledeci;
    }

    public void setSledeci(boolean sledeci) {
        this.sledeci = sledeci;
    }

    public Spojnice2() {
        drugaKolona = new String[10];
        for (int i = 0; i < 10; i++) {
            drugaKolona[i] = "?";
        }
        preostaloVreme = 60;
        sledecaPozicija = poeni = 0;
        bodovanje = false;
        pokrenut = false;
        brojPogodaka = 0;
        ok = new boolean[10];
        for (int i = 0; i < 10; i++) {
            ok[i] = true;
        }
        boja = new String[2][];
        for (int i = 0; i < 2; i++) {
            boja[i] = new String[10];
            for (int j = 0; j < 10; j++) {
                boja[i][j] = "belaBoja";
            }
        }
        tacnaKombinacija = new SPKlasa();
        sledeci = false;
        okZaSlanje = new boolean[2][];
        for (int i = 0; i < 2; i++) {
            okZaSlanje[i] = new boolean[10];
            for (int j = 0; j < 10; j++) {
                okZaSlanje[i][j] = false;
            }
        }

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        igraID = (int) hs.getAttribute("igraID");
        bojaIgraca = (String) hs.getAttribute("boja");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 3) {
            sledecaIgra();
        } else {
            stanje++;
        }

        Igra i = dohvatiIgru();
        if (daLiJeCrveniIgrac()) {
            ukupanBrojPoena = i.getPoeniCrvenog();
        }
        if (daLiJePlaviIgrac()) {
            ukupanBrojPoena = i.getPoeniPlavog();
        }
        //FacesContext.getCurrentInstance().responseComplete();

        Sinc sinc = dohvatiSinc();
        daLiPostojiJosJednaIgra = !sinc.isSledeciIgracCrveniSpojnice();

        if (daLiJePrviIgrac()) {
            List<entiteti.Spojnice> s;
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            Criteria cr = session.createCriteria(entiteti.Spojnice.class);
            s = cr.list();
            session.getTransaction().commit();
            session.close();

            int random = (new Random()).nextInt(s.size());

            spojnice = s.get(random).getSpoj();

            Sinc sinhr = dohvatiSinc();
            sinhr.setSpojnice(spojnice);
            azurirajSinc(sinhr);

        } else {
            Sinc sinhr = dohvatiSinc();
            while (sinhr.getSpojnice() == null) {
                sinhr = dohvatiSinc();
            }
            spojnice = sinhr.getSpojnice();
        }
    }

    public void sledecaIgra() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        hs.setAttribute("stanje", stanje);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("asocijacije.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Spojnice2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void zavrsiSpojnice() {
        if (!bodovanje) {
            boduj();
        }
        sledecaIgra();
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        sacuvajZavrsenuIgru();
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("rezultati.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Spojnice2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void kreni() {
        if (daLiJePrviIgrac()) {
            Random rand = new Random();
            LinkedList<Integer> lista = new LinkedList<>();
            int r = rand.nextInt(10);

            for (int i = 0; i < 10; i++) {
                while (lista.contains(r)) {
                    r = rand.nextInt(10);
                }
                lista.add(r);
                drugaKolona[i] = spojnice.kolona2[r];
            }
            Sinc sinc = dohvatiSinc();
            if (daLiJeCrveniIgrac()) {
                sinc.setOkSpojnice(null);
            }
            sinc.setDrugaKolona(drugaKolona);
            azurirajSinc(sinc);

            pokrenut = true;
            for (int i = 0; i < 10; i++) {
                ok[i] = false;
            }

            boja[0][sledecaPozicija] = "plavaBoja";
        } else {
            Sinc sinc = dohvatiSinc();
            while (sinc.getDrugaKolona() == null) {
                sinc = dohvatiSinc();
            }
            drugaKolona = sinc.getDrugaKolona();

            while (sinc.getOkSpojnice() == null) {
                sinc = dohvatiSinc();
            }

            boolean[][] okPrimljeni = sinc.getOkSpojnice();

            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 10; j++) {
                    if (okPrimljeni[i][j] == true) {
                        boja[i][j] = "zelenaBoja";
                    }
                }
            }

            ok = okPrimljeni[1];
            while (sledecaPozicija < 10 && okPrimljeni[0][sledecaPozicija] == true) {
                sledecaPozicija++;
            }
            if (sledecaPozicija != 10) {
                boja[0][sledecaPozicija] = "plavaBoja";
            }

            pokrenut = true;
        }
    }

    public void selektuj(int index) {
        if (drugaKolona[index].equals(spojnice.kolona2[sledecaPozicija])) {
            boja[0][sledecaPozicija] = "zelenaBoja";
            boja[1][index] = "zelenaBoja";
            okZaSlanje[0][sledecaPozicija] = true;
            okZaSlanje[1][index] = true;
            ok[index] = true;
            brojPogodaka++;
        } else {
            boja[0][sledecaPozicija] = "crvenaBoja";
        }
        sledecaPozicija++;
        while (sledecaPozicija < 10 && boja[0][sledecaPozicija].equals("zelenaBoja")) {
            sledecaPozicija++;
        }
        if (sledecaPozicija == 10) {
            for (int i = 0; i < 10; i++) {
                ok[i] = true;
            }
            if (!bodovanje) {
                boduj();
            }
        } else {
            boja[0][sledecaPozicija] = "plavaBoja";
        }
    }

    public void smanjiVreme() {
        if (pokrenut) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void boduj() {
        if (daLiJePrviIgrac()) {
            Sinc sinhr = dohvatiSinc();
            sinhr.setOkSpojnice(okZaSlanje);
            azurirajSinc(sinhr);
        }

        bodovanje = true;
        if (daLiJeSveURedu()) {
            poeni = brojPogodaka;
        }
        azurirajPoene();

        Sinc s = dohvatiSinc();
        if (s.isSledeciIgracCrveniSpojnice() == false) {
            s.setSledeciIgracCrveniSpojnice(true);
            s.setSpojnice(null);
            s.setDrugaKolona(null);
            azurirajSinc(s);
        }

        if (daLiPostojiJosJednaIgra) {
            predjiNaSledecegIgraca();
        }
    }

    public boolean daLiJeSveURedu() {
        //toDo
        return true;
    }

    public Sinc dohvatiSinc() {
        Sinhronizacija s;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Sinhronizacija.class);
        s = (Sinhronizacija) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return s.getSinc();
    }

    public void azurirajSinc(Sinc sinc) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Sinhronizacija s = (Sinhronizacija) session.get(Sinhronizacija.class, igraID);
        s.setSinc(sinc);
        session.update(s);

        session.getTransaction().commit();
        session.close();
    }

    public boolean daLiJePlaviIgrac() {
        return bojaIgraca.equals("plaviIgrac");
    }

    public boolean daLiJeCrveniIgrac() {
        return bojaIgraca.equals("crveniIgrac");
    }

    public Igra dohvatiIgru() {
        Igra i;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Igra.class);
        i = (Igra) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return i;
    }

    public void azurirajPoene() {
        if (daLiJePlaviIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setPlaviSpojnice(partija.getPlaviSpojnice() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniPlavog() + poeni;
            igra.setPoeniPlavog(igra.getPoeniPlavog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
        if (daLiJeCrveniIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setCrveniSpojnice(partija.getCrveniSpojnice() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniCrvenog() + poeni;
            igra.setPoeniCrvenog(igra.getPoeniCrvenog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
    }

    public void sacuvajZavrsenuIgru() {
        Igra i = dohvatiIgru();
        int poeniPlavog = i.getPoeniPlavog();
        int poeniCrvenog = i.getPoeniCrvenog();
        int ishod;
        if (poeniPlavog == poeniCrvenog) {
            ishod = 3;
        } else if (poeniPlavog > poeniCrvenog) {
            ishod = 1;
        } else {
            ishod = 2;
        }
        i.setIshod(ishod);
        i.setTip(3);

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        session.update(i);

        session.getTransaction().commit();
        session.close();
    }

    public void predjiNaSledecegIgraca() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        hs.setAttribute("stanje", stanje - 1);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("spojnice.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Spojnice2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public boolean daLiJePrviIgrac() {
        return (daLiJePlaviIgrac() && daLiPostojiJosJednaIgra) || (daLiJeCrveniIgrac() && !daLiPostojiJosJednaIgra);
    }

    public boolean sadIgram() {
        return (daLiJeCrveniIgrac() && !daLiJePrviIgrac() && sledeci == true) || (daLiJeCrveniIgrac() && daLiJePrviIgrac() && sledeci == false)
                || (daLiJePlaviIgrac() && !daLiJePrviIgrac() && sledeci == true) || (daLiJePlaviIgrac() && daLiJePrviIgrac() && sledeci == false);
    }
}
