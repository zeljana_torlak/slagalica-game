package kontroleri.partija;

import db.HibernateUtil;
import entiteti.Igra;
import entiteti.Korisnik;
import entiteti.Sinhronizacija;
import helper.AKlasa;
import helper.Partije;
import helper.Sinc;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Asocijacije2 {

    String korIme;
    Korisnik kor;
    int igraID;
    String bojaIgraca;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;
    int ukupanBrojPoena;

    AKlasa asocijacije;
    boolean pokrenut;
    int brojPogodaka;
    boolean[][] ok;
    String[][] boja;
    String[][] otvorenaPolja;
    String[] resenja;
    String[] bojeResenja;
    boolean[] okResenja;

    boolean selektovan;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public AKlasa getAsocijacije() {
        return asocijacije;
    }

    public void setAsocijacije(AKlasa asocijacije) {
        this.asocijacije = asocijacije;
    }

    public boolean isPokrenut() {
        return pokrenut;
    }

    public void setPokrenut(boolean pokrenut) {
        this.pokrenut = pokrenut;
    }

    public int getBrojPogodaka() {
        return brojPogodaka;
    }

    public void setBrojPogodaka(int brojPogodaka) {
        this.brojPogodaka = brojPogodaka;
    }

    public boolean[][] getOk() {
        return ok;
    }

    public void setOk(boolean[][] ok) {
        this.ok = ok;
    }

    public String[][] getBoja() {
        return boja;
    }

    public void setBoja(String[][] boja) {
        this.boja = boja;
    }

    public String[][] getOtvorenaPolja() {
        return otvorenaPolja;
    }

    public void setOtvorenaPolja(String[][] otvorenaPolja) {
        this.otvorenaPolja = otvorenaPolja;
    }

    public String[] getResenja() {
        return resenja;
    }

    public void setResenja(String[] resenja) {
        this.resenja = resenja;
    }

    public String[] getBojeResenja() {
        return bojeResenja;
    }

    public void setBojeResenja(String[] bojeResenja) {
        this.bojeResenja = bojeResenja;
    }

    public boolean[] getOkResenja() {
        return okResenja;
    }

    public void setOkResenja(boolean[] okResenja) {
        this.okResenja = okResenja;
    }

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public String getBojaIgraca() {
        return bojaIgraca;
    }

    public void setBojaIgraca(String bojaIgraca) {
        this.bojaIgraca = bojaIgraca;
    }

    public int getUkupanBrojPoena() {
        return ukupanBrojPoena;
    }

    public void setUkupanBrojPoena(int ukupanBrojPoena) {
        this.ukupanBrojPoena = ukupanBrojPoena;
    }

    public Asocijacije2() {
        preostaloVreme = 240;
        poeni = 0;
        bodovanje = false;
        pokrenut = false;
        brojPogodaka = 0;
        boja = new String[4][];
        ok = new boolean[4][];
        otvorenaPolja = new String[4][];
        for (int i = 0; i < 4; i++) {
            boja[i] = new String[4];
            ok[i] = new boolean[4];
            otvorenaPolja[i] = new String[4];
            for (int j = 0; j < 4; j++) {
                boja[i][j] = "belaBoja";
                ok[i][j] = true;
                otvorenaPolja[i][j] = "?";
            }
        }
        resenja = new String[5];
        bojeResenja = new String[5];
        okResenja = new boolean[5];
        for (int i = 0; i < 5; i++) {
            bojeResenja[i] = "belaBoja";
            okResenja[i] = true;
        }
        selektovan = false;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        igraID = (int) hs.getAttribute("igraID");
        bojaIgraca = (String) hs.getAttribute("boja");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 4) {
            zavrsiIgru();
        }

        Igra i = dohvatiIgru();
        if (daLiJeCrveniIgrac()) {
            ukupanBrojPoena = i.getPoeniCrvenog();
        }
        if (daLiJePlaviIgrac()) {
            ukupanBrojPoena = i.getPoeniPlavog();
        }
        //FacesContext.getCurrentInstance().responseComplete();

        if (daLiJePlaviIgrac()) {
            List<entiteti.Asocijacije> a;
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            Criteria cr = session.createCriteria(entiteti.Asocijacije.class);
            a = cr.list();
            session.getTransaction().commit();
            session.close();

            int random = (new Random()).nextInt(a.size());

            asocijacije = a.get(random).getAsoc();

            Sinc sinhr = dohvatiSinc();
            sinhr.setAsocijacije(asocijacije);
            azurirajSinc(sinhr);

        } else {
            Sinc sinhr = dohvatiSinc();
            while (sinhr.getAsocijacije() == null) {
                sinhr = dohvatiSinc();
            }
            asocijacije = sinhr.getAsocijacije();
        }
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        sacuvajZavrsenuIgru();
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("rezultati.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Asocijacije2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void kreni() {
        if (daLiJePlaviIgrac()) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    ok[i][j] = false;
                }
            }
            for (int i = 0; i < 5; i++) {
                okResenja[i] = false;
            }
        }
        if (daLiJeCrveniIgrac()) {
            for (int i = 0; i < 5; i++) {
                okResenja[i] = false;
            }
            Sinc sinc = dohvatiSinc();
            while (sinc.getOkAsocijacija() == null) {
                sinc = dohvatiSinc();
            }
            ok = sinc.getOkAsocijacija();
            boja = sinc.getBojeAsocijacija();
            otvorenaPolja = sinc.getOtvorenaPoljaAsocijacija();
            okResenja = sinc.getOkResenjaAsocijacija();
            bojeResenja = sinc.getBojeResenjaAsocijacija();
            resenja = sinc.getOtvorenaResenjaAsocijacija();
        }
        pokrenut = true;
    }

    public void selektuj(int index1, int index2) {
        if (!selektovan) {
            selektovan = true;
            otvorenaPolja[index1][index2] = asocijacije.kolone[index1][index2];
            boja[index1][index2] = "sivaBoja";
            ok[index1][index2] = true;
        }
    }

    public void gadjaj(int index) {
        if (index < 4) {
            if (resenja[index].equals(asocijacije.resenjaKolona[index])) {
                if (daLiJeCrveniIgrac()) {
                    bojeResenja[index] = "crvenaBoja";
                } else {
                    bojeResenja[index] = "plavaBoja";
                }
                okResenja[index] = true;
                brojPogodaka++;
                for (int i = 0; i < 4; i++) {
                    if (daLiJeCrveniIgrac()) {
                        boja[index][i] = "crvenaBoja";
                    } else {
                        boja[index][i] = "plavaBoja";
                    }
                    ok[index][i] = true;
                    otvorenaPolja[index][i] = asocijacije.kolone[index][i];
                }
            }
        } else {
            if (asocijacije.konacno.contains(resenja[index])) {
                if (daLiJeCrveniIgrac()) {
                    bojeResenja[index] = "crvenaBoja";
                } else {
                    bojeResenja[index] = "plavaBoja";
                }
                for (int i = 0; i < 5; i++) {
                    okResenja[i] = true;
                }
                brojPogodaka += 2;
                for (int i = 0; i < 4; i++) {
                    if (!bojeResenja[i].equals("crvenaBoja") && !bojeResenja[i].equals("plavaBoja")) {
                        brojPogodaka++;

                        if (daLiJeCrveniIgrac()) {
                            bojeResenja[i] = "crvenaBoja";
                        } else {
                            bojeResenja[i] = "plavaBoja";
                        }
                        for (int j = 0; j < 4; j++) {
                            if (daLiJeCrveniIgrac()) {
                                boja[i][j] = "crvenaBoja";
                            } else {
                                boja[i][j] = "plavaBoja";
                            }
                            ok[i][j] = true;
                            otvorenaPolja[i][j] = asocijacije.kolone[i][j];
                        }
                    }
                }
                if (!bodovanje) {
                    boduj();
                }
            }
        }
        Sinc sinc = dohvatiSinc();
        sinc.setDaLiJePrviIgracAsocijacije(daLiJeCrveniIgrac());
        sinc.setOkAsocijacija(ok);
        sinc.setBojeAsocijacija(boja);
        sinc.setOtvorenaPoljaAsocijacija(otvorenaPolja);
        sinc.setOkResenjaAsocijacija(okResenja);
        sinc.setBojeResenjaAsocijacija(bojeResenja);
        sinc.setOtvorenaResenjaAsocijacija(resenja);
        azurirajSinc(sinc);
        selektovan = false;
    }

    public void smanjiVreme() {
        if (pokrenut) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
                if (!sadIgram() && !bodovanje) {
                    while (!sadIgram() && !bodovanje);
                    Sinc sinc = dohvatiSinc();
                    ok = sinc.getOkAsocijacija();
                    boja = sinc.getBojeAsocijacija();
                    otvorenaPolja = sinc.getOtvorenaPoljaAsocijacija();
                    okResenja = sinc.getOkResenjaAsocijacija();
                    bojeResenja = sinc.getBojeResenjaAsocijacija();
                    resenja = sinc.getOtvorenaResenjaAsocijacija();
                }
            }
        }
    }

    public void boduj() {
        bodovanje = true;
        if (daLiJeSveURedu()) {
            poeni = brojPogodaka * 5;
        }
        azurirajPoene();
    }

    public boolean daLiJeSveURedu() {
        //toDo
        return true;
    }

    public Sinc dohvatiSinc() {
        Sinhronizacija s;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Sinhronizacija.class);
        s = (Sinhronizacija) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return s.getSinc();
    }

    public void azurirajSinc(Sinc sinc) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Sinhronizacija s = (Sinhronizacija) session.get(Sinhronizacija.class, igraID);
        s.setSinc(sinc);
        session.update(s);

        session.getTransaction().commit();
        session.close();
    }

    public boolean daLiJePlaviIgrac() {
        return bojaIgraca.equals("plaviIgrac");
    }

    public boolean daLiJeCrveniIgrac() {
        return bojaIgraca.equals("crveniIgrac");
    }

    public Igra dohvatiIgru() {
        Igra i;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Igra.class);
        i = (Igra) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return i;
    }

    public void azurirajPoene() {
        if (daLiJePlaviIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setPlaviAsocijacije(partija.getPlaviAsocijacije() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniPlavog() + poeni;
            igra.setPoeniPlavog(igra.getPoeniPlavog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
        if (daLiJeCrveniIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setCrveniAsocijacije(partija.getCrveniAsocijacije() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniCrvenog() + poeni;
            igra.setPoeniCrvenog(igra.getPoeniCrvenog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
    }

    public void sacuvajZavrsenuIgru() {
        Igra i = dohvatiIgru();
        int poeniPlavog = i.getPoeniPlavog();
        int poeniCrvenog = i.getPoeniCrvenog();
        int ishod;
        if (poeniPlavog == poeniCrvenog) {
            ishod = 3;
        } else if (poeniPlavog > poeniCrvenog) {
            ishod = 1;
        } else {
            ishod = 2;
        }
        i.setIshod(ishod);
        i.setTip(3);

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        session.update(i);

        session.getTransaction().commit();
        session.close();
    }

    public boolean sadIgram() {
        Sinc sinc = dohvatiSinc();
        return (daLiJeCrveniIgrac() && !sinc.isDaLiJePrviIgracAsocijacije()) || (daLiJePlaviIgrac() && sinc.isDaLiJePrviIgracAsocijacije());
    }

}
