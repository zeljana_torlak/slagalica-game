package kontroleri.partija;

import db.HibernateUtil;
import entiteti.Korisnik;
import entiteti.Sinhronizacija;
import entiteti.Igra;
import helper.Partije;
import helper.RandomGenerator;
import helper.SLKlasa;
import helper.Sinc;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Slagalica2 {

    String korIme;
    Korisnik kor;
    int igraID;
    String bojaIgraca;
    String rec;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;
    int ukupanBrojPoena;

    RandomGenerator rand;
    int sledeceSlovo;
    boolean[] ok;
    String[] slova;
    int sledecaPozicija;
    int[] pozicije;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public String getBojaIgraca() {
        return bojaIgraca;
    }

    public void setBojaIgraca(String bojaIgraca) {
        this.bojaIgraca = bojaIgraca;
    }

    public int getSledeceSlovo() {
        return sledeceSlovo;
    }

    public void setSledeceSlovo(int sledeceSlovo) {
        this.sledeceSlovo = sledeceSlovo;
    }

    public boolean[] getOk() {
        return ok;
    }

    public void setOk(boolean[] ok) {
        this.ok = ok;
    }

    public String[] getSlova() {
        return slova;
    }

    public void setSlova(String[] slova) {
        this.slova = slova;
    }

    public String getRec() {
        return rec;
    }

    public void setRec(String rec) {
        this.rec = rec;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public int getSledecaPozicija() {
        return sledecaPozicija;
    }

    public void setSledecaPozicija(int sledecaPozicija) {
        this.sledecaPozicija = sledecaPozicija;
    }

    public int[] getPozicije() {
        return pozicije;
    }

    public void setPozicije(int[] pozicije) {
        this.pozicije = pozicije;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public int getUkupanBrojPoena() {
        return ukupanBrojPoena;
    }

    public void setUkupanBrojPoena(int ukupanBrojPoena) {
        this.ukupanBrojPoena = ukupanBrojPoena;
    }

    public Slagalica2() {
        ok = new boolean[12];
        for (int i = 0; i < 12; i++) {
            ok[i] = true;
        }
        slova = new String[12];
        for (int i = 0; i < 12; i++) {
            slova[i] = "?";
        }
        rec = "";
        preostaloVreme = 60;
        sledecaPozicija = poeni = 0;
        pozicije = new int[12];
        bodovanje = false;
        rand = new RandomGenerator();

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        igraID = (int) hs.getAttribute("igraID");
        bojaIgraca = (String) hs.getAttribute("boja");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 0) {
            sledecaIgra();
        } else {
            stanje++;
        }

        Igra i = dohvatiIgru();
        if (daLiJeCrveniIgrac()) {
            ukupanBrojPoena = i.getPoeniCrvenog();
        }
        if (daLiJePlaviIgrac()) {
            ukupanBrojPoena = i.getPoeniPlavog();
        }
        //FacesContext.getCurrentInstance().responseComplete();
    }

    public void sledecaIgra() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        hs.setAttribute("stanje", stanje);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("mojBroj.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Slagalica2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void zavrsiSlagalicu() {
        if (!bodovanje) {
            boduj();
        }
        sledecaIgra();
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        sacuvajZavrsenuIgru();
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", bojaIgraca);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("rezultati.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Slagalica2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void stopSlova() {
        if (daLiJePlaviIgrac()) {
            slova[sledeceSlovo] = rand.randomSlovo();
            ok[sledeceSlovo] = false;
            sledeceSlovo++;

            Sinc sinc = dohvatiSinc();
            SLKlasa slagalica = sinc.getSlagalica();
            slagalica.slova[sledeceSlovo - 1] = slova[sledeceSlovo - 1];
            sinc.setSlagalica(slagalica);
            azurirajSinc(sinc);
        }
        if (daLiJeCrveniIgrac()) {
            if (sledeceSlovo < 12) {
                Sinc sinc = dohvatiSinc();
                SLKlasa slagalica = sinc.getSlagalica();

                while (slagalica.slova[sledeceSlovo].equals("")) {
                    sinc = dohvatiSinc();
                    slagalica = sinc.getSlagalica();
                }
                slova[sledeceSlovo] = slagalica.slova[sledeceSlovo];
                ok[sledeceSlovo] = false;
                sledeceSlovo++;
            }
        }
    }

    public void dodaj(int indexSlova) {
        rec = rec + slova[indexSlova];
        pozicije[sledecaPozicija] = indexSlova;
        ok[indexSlova] = true;
        sledecaPozicija++;
    }

    public void brisi() {
        sledecaPozicija--;
        int indexSlova = pozicije[sledecaPozicija];
        ok[indexSlova] = false;
        rec = rec.substring(0, rec.length() - slova[indexSlova].length());
    }

    public void smanjiVreme() {
        if (sledeceSlovo > 11) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void boduj() {
        bodovanje = true;
        if (daLiJeSveURedu()) {
            poeni = 2 * sledecaPozicija;
        }

        if (daLiJePlaviIgrac()) {
            Sinc sinc1 = dohvatiSinc();
            sinc1.setBrojPoenaPlavogIgracaSlagalica(poeni);
            azurirajSinc(sinc1);

            sinc1 = dohvatiSinc();
            while (sinc1.isPoeniSlagalica() == false) {
                sinc1 = dohvatiSinc();
            }

            poeni = sinc1.getBrojPoenaPlavogIgracaSlagalica();

            azurirajPoene();
        }
        if (daLiJeCrveniIgrac()) {
            Sinc sinc2 = dohvatiSinc();
            while (sinc2.getBrojPoenaPlavogIgracaSlagalica() == -1) {
                sinc2 = dohvatiSinc();
            }
            if (poeni == sinc2.getBrojPoenaPlavogIgracaSlagalica()) {
                if (poeni == 0) {
                    sinc2.setBrojPoenaPlavogIgracaSlagalica(0);
                }
            }
            if (poeni > sinc2.getBrojPoenaPlavogIgracaSlagalica()) {
                sinc2.setBrojPoenaPlavogIgracaSlagalica(0);
            }
            if (poeni < sinc2.getBrojPoenaPlavogIgracaSlagalica()) {
                poeni = 0;
            }
            sinc2.setPoeniSlagalica(true);
            azurirajSinc(sinc2);

            azurirajPoene();
        }
    }

    public boolean daLiJeSveURedu() {
        entiteti.Slagalica k;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(entiteti.Slagalica.class);
        k = (entiteti.Slagalica) cr.add(Restrictions.eq("rec", rec)).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return k != null;
    }

    public Sinc dohvatiSinc() {
        Sinhronizacija s;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Sinhronizacija.class);
        s = (Sinhronizacija) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return s.getSinc();
    }

    public void azurirajSinc(Sinc sinc) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Sinhronizacija s = (Sinhronizacija) session.get(Sinhronizacija.class, igraID);
        s.setSinc(sinc);
        session.update(s);

        session.getTransaction().commit();
        session.close();
    }

    public boolean daLiJePlaviIgrac() {
        return bojaIgraca.equals("plaviIgrac");
    }

    public boolean daLiJeCrveniIgrac() {
        return bojaIgraca.equals("crveniIgrac");
    }

    public Igra dohvatiIgru() {
        Igra i;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Igra.class);
        i = (Igra) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return i;
    }

    public void azurirajPoene() {
        if (daLiJePlaviIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setPlaviSlagalica(partija.getPlaviSlagalica() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniPlavog() + poeni;
            igra.setPoeniPlavog(igra.getPoeniPlavog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
        if (daLiJeCrveniIgrac()) {
            Igra igra = dohvatiIgru();
            Partije partija = igra.getPoeniPoPartijama();
            partija.setCrveniSlagalica(partija.getCrveniSlagalica() + poeni);
            igra.setPoeniPoPartijama(partija);

            ukupanBrojPoena = igra.getPoeniCrvenog() + poeni;
            igra.setPoeniCrvenog(igra.getPoeniCrvenog() + poeni);
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(igra);

            session.getTransaction().commit();
            session.close();
        }
    }

    public void sacuvajZavrsenuIgru() {
        Igra i = dohvatiIgru();
        int poeniPlavog = i.getPoeniPlavog();
        int poeniCrvenog = i.getPoeniCrvenog();
        int ishod;
        if (poeniPlavog == poeniCrvenog) {
            ishod = 3;
        } else if (poeniPlavog > poeniCrvenog) {
            ishod = 1;
        } else {
            ishod = 2;
        }
        i.setIshod(ishod);
        i.setTip(3);

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        session.update(i);

        session.getTransaction().commit();
        session.close();
    }
}
