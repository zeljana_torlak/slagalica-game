package kontroleri.partija;

import db.HibernateUtil;
import entiteti.Igra;
import entiteti.Korisnik;
import helper.Partije;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Rezultati {

    String korIme;
    Korisnik kor;
    int igraID;
    String bojaIgraca;
    String[][] podaci;
    String korImeProtivnika;

    public Rezultati() {
        podaci = new String[3][];
        for (int i = 0; i < 3; i++) {
            podaci[i] = new String[6];
        }
        podaci[0][0] = "Slagalica";
        podaci[0][1] = "Moj Broj";
        podaci[0][2] = "Skočko";
        podaci[0][3] = "Spojnice";
        podaci[0][4] = "Asocijacije";
        podaci[0][5] = "Ukupno";

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        igraID = (int) hs.getAttribute("igraID");
        bojaIgraca = (String) hs.getAttribute("boja");

        //FacesContext.getCurrentInstance().responseComplete();
        Igra igra = dohvatiIgru();
        Partije p = igra.getPoeniPoPartijama();

        podaci[1][0] = p.getPlaviSlagalica() + "";
        podaci[1][1] = p.getPlaviMojBroj() + "";
        podaci[1][2] = p.getPlaviSkocko() + "";
        podaci[1][3] = p.getPlaviSpojnice() + "";
        podaci[1][4] = p.getPlaviAsocijacije() + "";
        podaci[1][5] = igra.getPoeniPlavog() + "";

        podaci[2][0] = p.getCrveniSlagalica() + "";
        podaci[2][1] = p.getCrveniMojBroj() + "";
        podaci[2][2] = p.getCrveniSkocko() + "";
        podaci[2][3] = p.getCrveniSpojnice() + "";
        podaci[2][4] = p.getCrveniAsocijacije() + "";
        podaci[2][5] = igra.getPoeniCrvenog() + "";
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public String getBojaIgraca() {
        return bojaIgraca;
    }

    public void setBojaIgraca(String bojaIgraca) {
        this.bojaIgraca = bojaIgraca;
    }

    public String[][] getPodaci() {
        return podaci;
    }

    public void setPodaci(String[][] podaci) {
        this.podaci = podaci;
    }

    public void zavrsiPartiju() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/Projekat/faces/ucesnik.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Spojnice2.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public boolean daLiJePlaviIgrac() {
        return bojaIgraca.equals("plaviIgrac");
    }

    public boolean daLiJeCrveniIgrac() {
        return bojaIgraca.equals("crveniIgrac");
    }

    public Igra dohvatiIgru() {
        Igra i;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Igra.class);
        i = (Igra) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return i;
    }
}
