package kontroleri;

import db.HibernateUtil;
import entiteti.Asocijacije;
import entiteti.Korisnik;
import entiteti.Slagalica;
import entiteti.Spojnice;
import helper.AKlasa;
import helper.SPKlasa;
import java.util.LinkedList;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@ViewScoped
@ManagedBean
@SessionScoped
public class Supervizor {

    Korisnik kor;
    String korIme;

    String rec;
    SPKlasa spojnice;
    AKlasa asocijacije;

    String r;
    LinkedList<String> resenja;

    public Supervizor() {
        spojnice = new SPKlasa();
        asocijacije = new AKlasa();
        resenja = new LinkedList<>();

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        //FacesContext.getCurrentInstance().responseComplete();
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getRec() {
        return rec;
    }

    public void setRec(String rec) {
        this.rec = rec;
    }

    public SPKlasa getSpojnice() {
        return spojnice;
    }

    public void setSpojnice(SPKlasa spojnice) {
        this.spojnice = spojnice;
    }

    public AKlasa getAsocijacije() {
        return asocijacije;
    }

    public void setAsocijacije(AKlasa asocijacije) {
        this.asocijacije = asocijacije;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public LinkedList<String> getResenja() {
        return resenja;
    }

    public void setResenja(LinkedList<String> resenja) {
        this.resenja = resenja;
    }

    public void nesto() {
        r = "";
    }

    public void dodajRec() {
        if (!isAlpha(rec)) {
            rec = "";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Reč koju unosite mora da sadrži samo slova!"));
            return;
        }

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Slagalica s = new Slagalica();
        s.setRec(rec);

        session.save(s);

        session.getTransaction().commit();
        session.close();

        rec = "";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Uspešno ste dodali reč u bazu!"));
    }

    public void dodajSpojnice() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Spojnice s = new Spojnice();
        s.setSpoj(spojnice);

        session.save(s);

        session.getTransaction().commit();
        session.close();

        spojnice = new SPKlasa();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Uspešno ste dodali spojnice u bazu!"));
    }

    public void dodajAsocijacije() {
        resenja.add(r);
        asocijacije.setKonacno(resenja);

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Asocijacije a = new Asocijacije();
        a.setAsoc(asocijacije);
        session.save(a);

        session.getTransaction().commit();
        session.close();

        r = "";
        resenja = new LinkedList<>();
        asocijacije = new AKlasa();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Uspešno ste dodali asocijacije u bazu!"));
    }

    public boolean isAlpha(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }

        return true;
    }
}
