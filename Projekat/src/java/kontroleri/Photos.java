package kontroleri;

import db.HibernateUtil;
import entiteti.Korisnik;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.sql.rowset.serial.SerialBlob;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean
@ApplicationScoped //SessionScoped
public class Photos implements Serializable {

    StreamedContent image;

    public StreamedContent getImage() {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            //we're rendering the HTML - stub StreamedContent will generate right URL
            return new DefaultStreamedContent();
        } else {
            try {
                // browser is requesting the image - real StreamedContent with the image bytes
                String ID = context.getExternalContext().getRequestParameterMap().get("korisnikID");

                SessionFactory factory = HibernateUtil.getSessionFactory();
                Session session = factory.openSession();
                session.beginTransaction();

                Criteria cr = session.createCriteria(Korisnik.class);
                Korisnik korisnik = (Korisnik) cr.add(Restrictions.eq("korID", ID)).uniqueResult();
                session.getTransaction().commit();
                session.close();

                if (korisnik == null) {
                    return null;
                }

                SerialBlob b = new SerialBlob(korisnik.getSlika());
                return new DefaultStreamedContent(b.getBinaryStream(), "image/png");//jpg
            } catch (SQLException ex) {
                Logger.getLogger(Photos.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }

    StreamedContent image2;

    public StreamedContent getImage2() {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            //we're rendering the HTML - stub StreamedContent will generate right URL
            return new DefaultStreamedContent();
        } else {
            // browser is requesting the image - real StreamedContent with the image bytes
            String simbol = context.getExternalContext().getRequestParameterMap().get("skocko");

            if (simbol.equals("")) {
                InputStream stream = this.getClass().getResourceAsStream("images/image.png");
                return new DefaultStreamedContent(stream, "image/png");
            }

            InputStream stream = this.getClass().getResourceAsStream("images/" + simbol + ".png");
            return new DefaultStreamedContent(stream, "image/png");
        }
    }

}
