package kontroleri;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

@ManagedBean
@RequestScoped
public class Logout {

    public void logout() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/Projekat/faces/home.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Logout.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
//        FacesContext.getCurrentInstance().responseComplete();
//        FacesContext fc = FacesContext.getCurrentInstance();
//        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
//        s.invalidate();
//        return "home";
    }
}
