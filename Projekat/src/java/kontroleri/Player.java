package kontroleri;

import db.HibernateUtil;
import entiteti.Igra;
import entiteti.IgraDana;
import entiteti.Korisnik;
import entiteti.RezultatiIgreDana;
import entiteti.Sinhronizacija;
import helper.Partije;
import helper.Sinc;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Player {

    String korIme;
    Korisnik kor;
    int igraID;
    boolean cekanje;
    boolean topScore;
    String poruka;

    List<RezultatiIgreDana> rangLista;
    int rangKorisnika;
    List<Igra> odigranePartije;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public boolean isCekanje() {
        return cekanje;
    }

    public void setCekanje(boolean cekanje) {
        this.cekanje = cekanje;
    }

    public boolean isTopScore() {
        return topScore;
    }

    public void setTopScore(boolean topScore) {
        this.topScore = topScore;
    }

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }

    public List<RezultatiIgreDana> getRangLista() {
        return rangLista;
    }

    public void setRangLista(List<RezultatiIgreDana> rangLista) {
        this.rangLista = rangLista;
    }

    public int getRangKorisnika() {
        return rangKorisnika;
    }

    public void setRangKorisnika(int rangKorisnika) {
        this.rangKorisnika = rangKorisnika;
    }

    public List<Igra> getOdigranePartije() {
        return odigranePartije;
    }

    public void setOdigranePartije(List<Igra> odigranePartije) {
        this.odigranePartije = odigranePartije;
    }

    public Player() {
        cekanje = topScore = false;
        poruka = "";
        rangKorisnika = -1;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        //FacesContext.getCurrentInstance().responseComplete();
    }

    public void zapocni() {
        poruka = "";
        if (!proveri("korImePlavi") || !proveri("korImeCrveni")) {
            poruka = "Korisnik ima već započetu igru!";
            return;
        }

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Igra igra = new Igra();
        igra.setKorImePlavi(korIme);
        igra.setVreme(new Date());
        igra.setTip(1);
        igra.setPoeniPoPartijama(new Partije());

        session.save(igra);

        session.getTransaction().commit();
        session.close();

        igraID = igra.getIgraID();
        cekanje = true;
        poruka = "Igra je uspešno započeta!";

    }

    public void pridruziSe() {
        poruka = "";
        if (!proveri("korImePlavi") || !proveri("korImeCrveni")) {
            poruka = "Korisnik ima već započetu igru!";
            return;
        }

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Igra.class);
        Igra igra = (Igra) cr.add(Restrictions.eq("tip", 1)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        if (igra == null) {
            poruka = "Ne postoji nijedna započeta igra!";
            return;
        }

        SessionFactory factory2 = HibernateUtil.getSessionFactory();
        Session session2 = factory2.openSession();
        session2.beginTransaction();

        Igra igra2 = (Igra) session2.get(Igra.class, igra.getIgraID());
        igra2.setKorImeCrveni(korIme);
        igra2.setTip(2);
        session2.update(igra2);

        session2.getTransaction().commit();
        session2.close();

        igraID = igra2.getIgraID();

        napraviSinhronizaciju();

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("igraID", igraID);
        hs.setAttribute("boja", "crveniIgrac");
        hs.setAttribute("stanje", 0);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("partija/slagalica.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();

        poruka = "Uspešno ste se pridružili igri!";
    }

    public void igraDana() {
        poruka = "";
        SessionFactory factory2 = HibernateUtil.getSessionFactory();
        Session session2 = factory2.openSession();
        session2.beginTransaction();
        List<RezultatiIgreDana> lista;

        Criteria cr2 = session2.createCriteria(RezultatiIgreDana.class);
        lista = cr2.add(Restrictions.eq("korIme", korIme)).list();
        session2.getTransaction().commit();
        session2.close();

        boolean ok = true;
        for (int i = 0; i < lista.size(); i++) {
            if (isToday((Timestamp) lista.get(i).getVreme())) {
                ok = false;
                break;
            }
        }
        if (!ok) {
            poruka = "Već ste odigrali današnju Igru dana!";
            return;
        }

        IgraDana igrica = dohvatiIgruDana();
        if (igrica == null) {
            return;
        }

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        RezultatiIgreDana rid = new RezultatiIgreDana();
        rid.setKorIme(korIme);
        rid.setVreme(new Date());

        session.save(rid);

        session.getTransaction().commit();
        session.close();

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("rezultatiIgreDana", rid);
        hs.setAttribute("igraDana", igrica);
        hs.setAttribute("stanje", 0);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("igreDana/slagalica.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void rezultati() {
        topScore = true;
        rangLista = new LinkedList<>();

        List<RezultatiIgreDana> lista;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(RezultatiIgreDana.class);
        lista = cr.list();
        session.getTransaction().commit();
        session.close();

        for (int i = 0; i < lista.size(); i++) {
            if (isToday((Timestamp) lista.get(i).getVreme())) {
                rangLista.add(lista.get(i));
            }
        }

        rangLista.sort((o1, o2) -> o2.getPoeni() - o1.getPoeni());

        rangKorisnika = -1;
        for (int i = 0; i < rangLista.size(); i++) {
            if (rangLista.get(i).getKorIme().equals(korIme)) {
                rangKorisnika = i + 1;
                break;
            }
        }

        odigranePartije = new LinkedList<>();

        List<Igra> lista2;
        SessionFactory factory2 = HibernateUtil.getSessionFactory();
        Session session2 = factory2.openSession();
        session2.beginTransaction();

        Criteria cr2 = session2.createCriteria(Igra.class);
        lista2 = cr2.list();
        session2.getTransaction().commit();
        session2.close();

        for (int i = 0; i < lista2.size(); i++) {
            if ((lista2.get(i).getKorImePlavi().equals(korIme) || lista2.get(i).getKorImeCrveni().equals(korIme)) && daLiJePrethodnihDesetDana((Timestamp) lista2.get(i).getVreme())) {
                odigranePartije.add(lista2.get(i));
            }
        }
    }

    private boolean proveri(String s) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Igra.class);
        Igra igra = (Igra) cr.add(Restrictions.eq(s, korIme)).add(Restrictions.eq("tip", 1)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        SessionFactory factory2 = HibernateUtil.getSessionFactory();
        Session session2 = factory2.openSession();
        session2.beginTransaction();

        Criteria cr2 = session2.createCriteria(Igra.class);
        Igra igra2 = (Igra) cr2.add(Restrictions.eq(s, korIme)).add(Restrictions.eq("tip", 2)).uniqueResult();
        session2.getTransaction().commit();
        session2.close();

        return igra == null && igra2 == null;
    }

    public void gdeJeCrveni() {
        if (cekanje == true) {
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            Criteria cr = session.createCriteria(Igra.class);
            Igra igra = (Igra) cr.add(Restrictions.eq("igraID", igraID)).uniqueResult();
            session.getTransaction().commit();
            session.close();

            if (igra.getKorImeCrveni() != null) {
                cekanje = false;

                FacesContext fc = FacesContext.getCurrentInstance();
                HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
                hs.setAttribute("korisnik", kor);
                hs.setAttribute("igraID", igraID);
                hs.setAttribute("boja", "plaviIgrac");
                hs.setAttribute("stanje", 0);
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("partija/slagalica.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
                }
                FacesContext.getCurrentInstance().responseComplete();

                poruka = "Drugi igrač se pridružio!";
            }
        }
    }

    public boolean isToday(Timestamp t1) {
        Date d1 = new Date(t1.getTime());
        Date d2 = new Date();
        String s1 = d1.toString().substring(0, d1.toString().length() - 18);
        String s2 = d2.toString().substring(0, d2.toString().length() - 18);
        String s3 = d1.toString().substring(d1.toString().length() - 4, d1.toString().length());
        String s4 = d2.toString().substring(d2.toString().length() - 4, d2.toString().length());
        return s1.equals(s2) && s3.equals(s4);
    }

    private IgraDana dohvatiIgruDana() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();
        List<IgraDana> lista;

        Criteria cr = session.createCriteria(IgraDana.class);
        lista = cr.list();
        session.getTransaction().commit();
        session.close();

        IgraDana igra = null;

        boolean ok = false;
        for (int i = 0; i < lista.size(); i++) {
            if (isToday((Timestamp) lista.get(i).getDatum())) {
                ok = true;
                igra = lista.get(i);
                break;
            }
        }
        if (!ok) {
            poruka = "Nije definisana današnja Igra dana! Kontaktirajte administratora!";
            return null;
        }
        return igra;
    }

    private void napraviSinhronizaciju() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Sinhronizacija s = new Sinhronizacija();
        s.setIgraID(igraID);
        s.setSinc(new Sinc());

        session.save(s);

        session.getTransaction().commit();
        session.close();
    }

    public String protivnik(int index) {
        if (odigranePartije.get(index).getKorImePlavi().equals(korIme)) {
            return odigranePartije.get(index).getKorImeCrveni();
        } else {
            return odigranePartije.get(index).getKorImePlavi();
        }
    }

    public String ishod(int index) {
        int ishod = odigranePartije.get(index).getIshod();
        if (ishod == 3) {
            return "nerešeno";
        }
        if ((odigranePartije.get(index).getKorImePlavi().equals(korIme) && ishod == 1) || (odigranePartije.get(index).getKorImeCrveni().equals(korIme) && ishod == 2)) {
            return "Vaša pobeda";
        } else if ((odigranePartije.get(index).getKorImePlavi().equals(korIme) && ishod == 2) || (odigranePartije.get(index).getKorImeCrveni().equals(korIme) && ishod == 1)) {
            return "pobeda protivnika";
        } else {
            return "još uvek je u toku";
        }
    }

    private boolean daLiJePrethodnihDesetDana(Timestamp t) {
        Date d = new Date(t.getTime());
        Date today = new Date();
        long temp = 86400000 * 10;
        today.setTime(today.getTime() - temp);
        return d.after(today);
    }
}
