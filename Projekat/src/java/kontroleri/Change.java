package kontroleri;

import db.HibernateUtil;
import entiteti.Korisnik;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@RequestScoped
public class Change {

    String korIme;
    String lozinka;
    String nova;

    String poruka;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getNova() {
        return nova;
    }

    public void setNova(String nova) {
        this.nova = nova;
    }

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }

    public void change() {
        poruka = "";

        if (!proveri()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", poruka));
            return;
        }

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Korisnik korisnik = (Korisnik) session.get(Korisnik.class, korIme);
        try {
            korisnik.setLozinka(HashSHA2(nova));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        session.update(korisnik);

        session.getTransaction().commit();
        session.close();

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("faces/prijava.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String HashSHA2(String value) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(value.getBytes("UTF-8"));

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < encodedhash.length; i++) {
            builder.append(Integer.toString((encodedhash[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return builder.toString();
    }

    private boolean proveri() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Korisnik.class);
        Korisnik korisnik = (Korisnik) cr.add(Restrictions.eq("korIme", korIme)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        if (korisnik == null) {
            poruka = "Korisnik sa unetim podacima ne postoji u bazi!";
            return false;
        }

        try {
            if (korisnik.getLozinka() == null ? HashSHA2(lozinka) != null : !korisnik.getLozinka().equals(HashSHA2(lozinka))) {
                poruka = "Uneli ste pogrešnu lozinku!";
                return false;
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

}
