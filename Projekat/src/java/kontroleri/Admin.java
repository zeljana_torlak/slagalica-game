package kontroleri;

import db.HibernateUtil;
import entiteti.Asocijacije;
import entiteti.IgraDana;
import entiteti.Korisnik;
import entiteti.RezultatiIgreDana;
import entiteti.Spojnice;
import helper.AKlasa;
import helper.MBKlasa;
import helper.RandomGenerator;
import helper.SKKlasa;
import helper.SLKlasa;
import helper.SPKlasa;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Admin {

    Korisnik kor;
    String korIme;

    List<Korisnik> korisnici;
    int broj;

    boolean vecSeDesilo = false;

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public List<Korisnik> getKorisnici() {
        return korisnici;
    }

    public void setKorisnici(List<Korisnik> korisnici) {
        this.korisnici = korisnici;
    }

    public int getBroj() {
        return broj;
    }

    public void setBroj(int broj) {
        this.broj = broj;
    }

    public SLKlasa getSlagalica() {
        return slagalica;
    }

    public void setSlagalica(SLKlasa slagalica) {
        this.slagalica = slagalica;
    }

    public MBKlasa getMojBroj() {
        return mojBroj;
    }

    public void setMojBroj(MBKlasa mojBroj) {
        this.mojBroj = mojBroj;
    }

    public String[] getMbr() {
        return mbr;
    }

    public void setMbr(String[] mbr) {
        this.mbr = mbr;
    }

    public String[] getBr() {
        return br;
    }

    public void setBr(String[] br) {
        this.br = br;
    }

    public SKKlasa getSkocko() {
        return skocko;
    }

    public void setSkocko(SKKlasa skocko) {
        this.skocko = skocko;
    }

    public int getSledeciSimbol() {
        return sledeciSimbol;
    }

    public void setSledeciSimbol(int sledeciSimbol) {
        this.sledeciSimbol = sledeciSimbol;
    }

    public int getSledeceSlovo() {
        return sledeceSlovo;
    }

    public void setSledeceSlovo(int sledeceSlovo) {
        this.sledeceSlovo = sledeceSlovo;
    }

    public int getSledeciBroj() {
        return sledeciBroj;
    }

    public void setSledeciBroj(int sledeciBroj) {
        this.sledeciBroj = sledeciBroj;
    }

    public LinkedList<SPKlasa> getSpojnice() {
        return spojnice;
    }

    public void setSpojnice(LinkedList<SPKlasa> spojnice) {
        this.spojnice = spojnice;
    }

    public LinkedList<Integer> getSpojniceID() {
        return spojniceID;
    }

    public void setSpojniceID(LinkedList<Integer> spojniceID) {
        this.spojniceID = spojniceID;
    }

    public SPKlasa getSelektovaneSpojnice() {
        return selektovaneSpojnice;
    }

    public void setSelektovaneSpojnice(SPKlasa selektovaneSpojnice) {
        this.selektovaneSpojnice = selektovaneSpojnice;
    }

    public int getSpojID() {
        return spojID;
    }

    public void setSpojID(int spojID) {
        this.spojID = spojID;
    }

    public LinkedList<AKlasa> getAsocijacije() {
        return asocijacije;
    }

    public void setAsocijacije(LinkedList<AKlasa> asocijacije) {
        this.asocijacije = asocijacije;
    }

    public LinkedList<Integer> getAsocijacijeID() {
        return asocijacijeID;
    }

    public void setAsocijacijeID(LinkedList<Integer> asocijacijeID) {
        this.asocijacijeID = asocijacijeID;
    }

    public AKlasa getSelektovaneAsocijacije() {
        return selektovaneAsocijacije;
    }

    public void setSelektovaneAsocijacije(AKlasa selektovaneAsocijacije) {
        this.selektovaneAsocijacije = selektovaneAsocijacije;
    }

    public int getAsocID() {
        return asocID;
    }

    public void setAsocID(int asocID) {
        this.asocID = asocID;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public void provera() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Korisnik.class);
        korisnici = cr.add(Restrictions.eq("tip", 4)).list();
        broj = korisnici.size();
        session.getTransaction().commit();
        session.close();
    }

    public void prihvati(Korisnik kor) {
        String korIme = kor.getKorIme();
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Korisnik korisnik = (Korisnik) session.get(Korisnik.class, korIme);
        korisnik.setTip(1);
        session.update(korisnik);

        session.getTransaction().commit();
        session.close();

        provera();
    }

    public void odbij(Korisnik kor) {
        String korIme = kor.getKorIme();
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Korisnik korisnik = (Korisnik) session.get(Korisnik.class, korIme);
        session.delete(korisnik);

        session.getTransaction().commit();
        session.close();

        provera();
    }

    SLKlasa slagalica;
    MBKlasa mojBroj;
    SKKlasa skocko;
    String[] mbr;
    String[] br;
    int sledeceSlovo;
    int sledeciBroj;
    int sledeciSimbol;
    RandomGenerator rand;
    LinkedList<SPKlasa> spojnice;
    LinkedList<Integer> spojniceID;
    SPKlasa selektovaneSpojnice;
    int spojID;
    LinkedList<AKlasa> asocijacije;
    LinkedList<Integer> asocijacijeID;
    AKlasa selektovaneAsocijacije;
    int asocID;
    Date datum;

    public Admin() {
        slagalica = new SLKlasa();
        mojBroj = new MBKlasa();
        skocko = new SKKlasa();
        rand = new RandomGenerator();
        mbr = new String[3];
        br = new String[6];
        sledeceSlovo = 0;
        sledeciBroj = 0;
        sledeciSimbol = 0;
        spojnice = new LinkedList<>();
        spojniceID = new LinkedList<>();
        spojID = 0;
        asocijacije = new LinkedList<>();
        asocijacijeID = new LinkedList<>();
        asocID = 0;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        //FacesContext.getCurrentInstance().responseComplete();

        List<Spojnice> s;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Spojnice.class);
        s = cr.list();
        session.getTransaction().commit();
        session.close();

        if (s.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "U bazi ne postoje unete spojnice!"));
        } else {
            for (int i = 0; i < s.size(); i++) {
                spojnice.add(s.get(i).getSpoj());
                spojniceID.add(s.get(i).getSpojID());
            }
        }

        List<Asocijacije> a;
        SessionFactory factory2 = HibernateUtil.getSessionFactory();
        Session session2 = factory2.openSession();
        session2.beginTransaction();

        Criteria cr2 = session2.createCriteria(Asocijacije.class);
        a = cr2.list();
        session2.getTransaction().commit();
        session2.close();

        if (a.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "U bazi ne postoje unete asocijacije!"));
        } else {
            for (int i = 0; i < a.size(); i++) {
                asocijacije.add(a.get(i).getAsoc());
                asocijacijeID.add(a.get(i).getAsocID());
            }
        }
    }

    public void stopSlova() {
        slagalica.slova[sledeceSlovo] = rand.randomSlovo();
        sledeceSlovo++;
    }

    public void stopBroj() {
        switch (sledeciBroj) {
            case 0:
                mbr[sledeciBroj] = rand.randomMaliBroj() + "";
                break;
            case 1:
            case 2:
                mbr[sledeciBroj] = rand.randomBroj() + "";
                break;
            case 3:
            case 4:
            case 5:
            case 6:
                br[sledeciBroj - 3] = (mojBroj.brojevi[sledeciBroj - 3] = rand.randomMaliBroj()) + "";
                break;
            case 7:
                br[4] = (mojBroj.brojevi[4] = rand.randomSrednjiBroj()) + "";
                break;
            case 8:
                br[5] = (mojBroj.brojevi[5] = rand.randomVelikiBroj()) + "";
                mojBroj.mojBroj = Integer.parseInt(mbr[0] + mbr[1] + mbr[2]);
                break;
            default:
                break;
        }
        sledeciBroj++;
    }

    public void stopSimbol() {
        skocko.kombinacija[sledeciSimbol] = rand.randomSimbol();
        sledeciSimbol++;
    }

    public void izaberiSpojnice() {
        selektovaneSpojnice = spojnice.get(spojID - 1);
    }

    public void izaberiAsocijacije() {
        selektovaneAsocijacije = asocijacije.get(asocID - 1);
    }

    public void unesiIgruDana() {
        boolean ok = true;
        if (sledeceSlovo <= 11) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Niste uneli neophodne podatke za igru Slagalica!"));
            ok = false;
        }
        if (sledeciBroj <= 8) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Niste uneli neophodne podatke za igru Moj broj!"));
            ok = false;
        }
        if (sledeciSimbol <= 3) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Niste uneli neophodne podatke za igru Skočko!"));
            ok = false;
        }
        if (selektovaneSpojnice == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Niste uneli neophodne podatke za igru Spojnice!"));
            ok = false;
        }
        if (selektovaneAsocijacije == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Niste uneli neophodne podatke za igru Asocijacije!"));
            ok = false;
        }
        if (datum.before(new Date()) && !isToday()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Izabrani datum je u prošlosti!"));
            ok = false;
        }
        if (!proveraDatuma() && !daLiAzurirati()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "U bazi već postoji odigrana igra dana za izabrani datum!"));
            ok = false;
        }
        if (!ok) {
            return;
        }

        IgraDana igraDana = new IgraDana();
        boolean azuriranje = daLiAzurirati();
        if (azuriranje) {
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            Criteria cr = session.createCriteria(IgraDana.class);
            igraDana = (IgraDana) cr.add(Restrictions.eq("datum", datum)).uniqueResult();
            if (igraDana==null) {
                igraDana = new IgraDana();
                azuriranje = false;
            }
            session.getTransaction().commit();
            session.close();
        }

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        igraDana.setSlagalica(slagalica);
        igraDana.setMojBroj(mojBroj);
        igraDana.setSkocko(skocko);
        igraDana.setSpojID(spojID);
        igraDana.setAsocID(asocID);
        igraDana.setDatum(datum);

        if (azuriranje) {
            session.update(igraDana);
        } else {
            session.save(igraDana);
        }

        session.getTransaction().commit();
        session.close();

        sledeceSlovo = sledeciBroj = sledeciSimbol = 0;
        selektovaneSpojnice = null;
        selektovaneAsocijacije = null;

        slagalica = new SLKlasa();
        mojBroj = new MBKlasa();
        skocko = new SKKlasa();
        mbr = new String[3];
        br = new String[6];
        spojID = asocID = 0;

        if (azuriranje) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Uspešno ste ažurirali igru dana!"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Uspešno ste uneli igru dana!"));
        }
    }

    public boolean proveraDatuma() {
        IgraDana i;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(IgraDana.class);
        i = (IgraDana) cr.add(Restrictions.eq("datum", datum)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return i == null;
    }

    public boolean daLiAzurirati() {
        List<RezultatiIgreDana> lista;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(RezultatiIgreDana.class);
        lista = cr.list();
        session.getTransaction().commit();
        session.close();

        boolean ok = true;
        for (int i = 0; i < lista.size(); i++) {
            if (isSameDay((Timestamp) lista.get(i).getVreme(), datum)) {
                ok = false;
                break;
            }
        }
        return ok;
    }

    public boolean isToday() {
        Date d2 = new Date();
        String s1 = datum.toString().substring(0, datum.toString().length() - 18);
        String s2 = d2.toString().substring(0, d2.toString().length() - 18);
        String s3 = datum.toString().substring(datum.toString().length() - 4, datum.toString().length());
        String s4 = d2.toString().substring(d2.toString().length() - 4, d2.toString().length());
        return s1.equals(s2) && s3.equals(s4);
    }

    public boolean isSameDay(Timestamp t1, Date d2) {
        Date d1 = new Date(t1.getTime());
        String s1 = d1.toString().substring(0, d1.toString().length() - 18);
        String s2 = d2.toString().substring(0, d2.toString().length() - 18);
        String s3 = d1.toString().substring(d1.toString().length() - 4, d1.toString().length());
        String s4 = d2.toString().substring(d2.toString().length() - 4, d2.toString().length());
        return s1.equals(s2) && s3.equals(s4);
    }
}
