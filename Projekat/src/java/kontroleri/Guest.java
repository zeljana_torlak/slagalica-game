package kontroleri;

import db.HibernateUtil;
import entiteti.Igra;
import helper.RangLista;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@ManagedBean
@RequestScoped
public class Guest {

    LinkedList<RangLista> rangLista;
    boolean daLiJeRangListaZaMesec;

    public Guest() {
        rangLista = new LinkedList<>();
        dohvRangListuZaPrethodnuNedelju();
    }

    public LinkedList<RangLista> getRangLista() {
        return rangLista;
    }

    public void setRangLista(LinkedList<RangLista> rangLista) {
        this.rangLista = rangLista;
    }

    public boolean isDaLiJeRangListaZaMesec() {
        return daLiJeRangListaZaMesec;
    }

    public void setDaLiJeRangListaZaMesec(boolean daLiJeRangListaZaMesec) {
        this.daLiJeRangListaZaMesec = daLiJeRangListaZaMesec;
    }

    public void dohvRangListu() {
        List<Igra> lista;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Igra.class);
        lista = cr.list();
        session.getTransaction().commit();
        session.close();

        if (daLiJeRangListaZaMesec == true) {
            for (int i = 0; i < lista.size(); i++) {
                if (!daLiJePrethodniMesec((Timestamp) lista.get(i).getVreme())){
                    lista.remove(i);
                    i--;
                }
            }
        } else {
            for (int i = 0; i < lista.size(); i++) {
                if (!daLiJePrethodnaNedelja((Timestamp) lista.get(i).getVreme())){
                    lista.remove(i);
                    i--;
                }
            }
        }

        LinkedList<String> igraci = new LinkedList<>();
        for (int i = 0; i < lista.size(); i++) {
            if (!igraci.contains(lista.get(i).getKorImePlavi())) {
                igraci.add(lista.get(i).getKorImePlavi());
                RangLista novi = new RangLista();
                novi.setKorIme(lista.get(i).getKorImePlavi());
                novi.setPoeni(lista.get(i).getPoeniPlavog());
                rangLista.add(novi);
            } else {
                azuriraj(lista.get(i).getKorImePlavi(), lista.get(i).getPoeniPlavog());
            }
            if (!igraci.contains(lista.get(i).getKorImeCrveni())) {
                igraci.add(lista.get(i).getKorImeCrveni());
                RangLista novi = new RangLista();
                novi.setKorIme(lista.get(i).getKorImeCrveni());
                novi.setPoeni(lista.get(i).getPoeniCrvenog());
                rangLista.add(novi);
            } else {
                azuriraj(lista.get(i).getKorImeCrveni(), lista.get(i).getPoeniCrvenog());
            }
        }

        rangLista.sort((o1, o2) -> o2.getPoeni() - o1.getPoeni());
    }

    private void azuriraj(String korIme, int poeni) {
        for (int i = 0; i < rangLista.size(); i++) {
            if (rangLista.get(i).getKorIme().equals(korIme)) {
                rangLista.get(i).setPoeni(rangLista.get(i).getPoeni() + poeni);
                break;
            }
        }
    }

    public void dohvRangListuZaPrethodnuNedelju() {
        rangLista.clear();
        daLiJeRangListaZaMesec = false;
        dohvRangListu();
    }

    public void dohvRangListuZaPrethodniMesec() {
        rangLista.clear();
        daLiJeRangListaZaMesec = true;
        dohvRangListu();
    }

    private boolean daLiJePrethodniMesec(Timestamp t) {
        Date today = new Date();
        Date d = new Date(t.getTime());
        return d.getMonth() == today.getMonth() && d.getYear() == today.getYear();
    }

    private boolean daLiJePrethodnaNedelja(Timestamp t) {
        Date d = new Date(t.getTime());
        Date today = new Date();
        long temp = 86400000 * 7;
        today.setTime(today.getTime() - temp);
        return d.after(today);
    }
    
}
