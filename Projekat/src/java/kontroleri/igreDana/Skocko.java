package kontroleri.igreDana;

import db.HibernateUtil;
import entiteti.IgraDana;
import entiteti.Korisnik;
import entiteti.RezultatiIgreDana;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@ManagedBean
@SessionScoped
@ViewScoped
public class Skocko {

    public String[] naziviSimbola = {"skocko", "herc", "pik", "karo", "tref", "zvezda"};

    String korIme;
    Korisnik kor;
    RezultatiIgreDana rezultatiIgreDana;
    IgraDana igraDana;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;

    String[][] simboli;
    int sledeciRed;
    int brojSimbolaURedu;
    boolean pokrenut, kraj;
    String[] pogodak;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public RezultatiIgreDana getRezultatiIgreDana() {
        return rezultatiIgreDana;
    }

    public void setRezultatiIgreDana(RezultatiIgreDana rezultatiIgreDana) {
        this.rezultatiIgreDana = rezultatiIgreDana;
    }

    public IgraDana getIgraDana() {
        return igraDana;
    }

    public void setIgraDana(IgraDana igraDana) {
        this.igraDana = igraDana;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public String[][] getSimboli() {
        return simboli;
    }

    public void setSimboli(String[][] simboli) {
        this.simboli = simboli;
    }

    public int getSledeciRed() {
        return sledeciRed;
    }

    public void setSledeciRed(int sledeciRed) {
        this.sledeciRed = sledeciRed;
    }

    public int getBrojSimbolaURedu() {
        return brojSimbolaURedu;
    }

    public void setBrojSimbolaURedu(int brojSimbolaURedu) {
        this.brojSimbolaURedu = brojSimbolaURedu;
    }

    public boolean isPokrenut() {
        return pokrenut;
    }

    public void setPokrenut(boolean pokrenut) {
        this.pokrenut = pokrenut;
    }

    public String[] getPogodak() {
        return pogodak;
    }

    public void setPogodak(String[] pogodak) {
        this.pogodak = pogodak;
    }

    public String[] getNaziviSimbola() {
        return naziviSimbola;
    }

    public void setNaziviSimbola(String[] naziviSimbola) {
        this.naziviSimbola = naziviSimbola;
    }

    public boolean isKraj() {
        return kraj;
    }

    public void setKraj(boolean kraj) {
        this.kraj = kraj;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public Skocko() {
        simboli = new String[6][];
        for (int i = 0; i < 6; i++) {
            simboli[i] = new String[4];
        }
        preostaloVreme = 60;
        sledeciRed = poeni = 0;
        pokrenut = kraj = false;
        pogodak = new String[6];
        bodovanje = false;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        rezultatiIgreDana = (RezultatiIgreDana) hs.getAttribute("rezultatiIgreDana");
        igraDana = (IgraDana) hs.getAttribute("igraDana");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 2) {
            sledecaIgra();
        } else {
            stanje++;
        }
        //FacesContext.getCurrentInstance().responseComplete();
    }

    public void sledecaIgra() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("rezultatiIgreDana", rezultatiIgreDana);
        hs.setAttribute("igraDana", igraDana);
        hs.setAttribute("stanje", stanje);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("spojnice.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Skocko.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void zavrsiSkocka() {
        if (!bodovanje) {
            boduj();
        }
        sledecaIgra();
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/Projekat/faces/ucesnik.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Skocko.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void kreni() {
        pokrenut = true;
    }

    public void dodaj(int indexSimbola) {
        if (brojSimbolaURedu < 4) {
            simboli[sledeciRed][brojSimbolaURedu] = naziviSimbola[indexSimbola];
            brojSimbolaURedu++;
        }
    }

    public void brisi() {
        simboli[sledeciRed][brojSimbolaURedu - 1] = "";
        if (brojSimbolaURedu > 0) {
            brojSimbolaURedu--;
        }
    }

    public void proveri() {
        LinkedList<Integer> lista1 = new LinkedList<>();
        LinkedList<Integer> lista2 = new LinkedList<>();
        int crvena = 0;
        int zuta = 0;

        for (int i = 0; i < 4; i++) {
            if (simboli[sledeciRed][i].equals(igraDana.getSkocko().kombinacija[i])) {
                lista1.add(i);
                lista2.add(i);
                crvena++;
            }
        }

        for (int i = 0; i < 4; i++) {
            if (!lista1.contains(i)) {
                for (int j = 0; j < 4; j++) {
                    if (!lista2.contains(j) && simboli[sledeciRed][i].equals(igraDana.getSkocko().kombinacija[j])) {
                        lista2.add(j);
                        zuta++;
                        break;
                    }
                }
            }
        }

        sledeciRed++;
        brojSimbolaURedu = 0;
        pogodak[sledeciRed - 1] = "slika" + crvena + "" + zuta;

        if (daLiJeSveURedu()) {
            if (!bodovanje) {
                boduj();
            }
            kraj = true;
        }
        if (sledeciRed == 6) {
            kraj = true;
        }
    }

    public void smanjiVreme() {
        if (pokrenut) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void boduj() {
        bodovanje = true;
        if (daLiJeSveURedu()) {
            poeni = 10;
            rezultatiIgreDana.setPoeni(rezultatiIgreDana.getPoeni() + poeni);

            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(rezultatiIgreDana);

            session.getTransaction().commit();
            session.close();
        }
    }

    public boolean daLiJeSveURedu() {
        if (sledeciRed == 0) {
            return false;
        }
        for (int i = 0; i < 4; i++) {
            if (!simboli[sledeciRed - 1][i].equals(igraDana.getSkocko().kombinacija[i])) {
                return false;
            }
        }
        return true;
    }
}
