package kontroleri.igreDana;

import db.HibernateUtil;
import entiteti.IgraDana;
import entiteti.Korisnik;
import entiteti.RezultatiIgreDana;
import helper.AKlasa;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Asocijacije {

    String korIme;
    Korisnik kor;
    RezultatiIgreDana rezultatiIgreDana;
    IgraDana igraDana;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;

    AKlasa asocijacije;
    boolean pokrenut;
    int brojPogodaka;
    boolean[][] ok;
    String[][] boja;
    String[][] otvorenaPolja;
    String[] resenja;
    String[] bojeResenja;
    boolean[] okResenja;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public RezultatiIgreDana getRezultatiIgreDana() {
        return rezultatiIgreDana;
    }

    public void setRezultatiIgreDana(RezultatiIgreDana rezultatiIgreDana) {
        this.rezultatiIgreDana = rezultatiIgreDana;
    }

    public IgraDana getIgraDana() {
        return igraDana;
    }

    public void setIgraDana(IgraDana igraDana) {
        this.igraDana = igraDana;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public AKlasa getAsocijacije() {
        return asocijacije;
    }

    public void setAsocijacije(AKlasa asocijacije) {
        this.asocijacije = asocijacije;
    }

    public boolean isPokrenut() {
        return pokrenut;
    }

    public void setPokrenut(boolean pokrenut) {
        this.pokrenut = pokrenut;
    }

    public int getBrojPogodaka() {
        return brojPogodaka;
    }

    public void setBrojPogodaka(int brojPogodaka) {
        this.brojPogodaka = brojPogodaka;
    }

    public boolean[][] getOk() {
        return ok;
    }

    public void setOk(boolean[][] ok) {
        this.ok = ok;
    }

    public String[][] getBoja() {
        return boja;
    }

    public void setBoja(String[][] boja) {
        this.boja = boja;
    }

    public String[][] getOtvorenaPolja() {
        return otvorenaPolja;
    }

    public void setOtvorenaPolja(String[][] otvorenaPolja) {
        this.otvorenaPolja = otvorenaPolja;
    }

    public String[] getResenja() {
        return resenja;
    }

    public void setResenja(String[] resenja) {
        this.resenja = resenja;
    }

    public String[] getBojeResenja() {
        return bojeResenja;
    }

    public void setBojeResenja(String[] bojeResenja) {
        this.bojeResenja = bojeResenja;
    }

    public boolean[] getOkResenja() {
        return okResenja;
    }

    public void setOkResenja(boolean[] okResenja) {
        this.okResenja = okResenja;
    }

    public Asocijacije() {
        preostaloVreme = 240;
        poeni = 0;
        bodovanje = false;
        pokrenut = false;
        brojPogodaka = 0;
        boja = new String[4][];
        ok = new boolean[4][];
        otvorenaPolja = new String[4][];
        for (int i = 0; i < 4; i++) {
            boja[i] = new String[4];
            ok[i] = new boolean[4];
            otvorenaPolja[i] = new String[4];
            for (int j = 0; j < 4; j++) {
                boja[i][j] = "belaBoja";
                ok[i][j] = true;
                otvorenaPolja[i][j] = "?";
            }
        }
        resenja = new String[5];
        bojeResenja = new String[5];
        okResenja = new boolean[5];
        for (int i = 0; i < 5; i++) {
            bojeResenja[i] = "belaBoja";
            okResenja[i] = true;
        }

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        rezultatiIgreDana = (RezultatiIgreDana) hs.getAttribute("rezultatiIgreDana");
        igraDana = (IgraDana) hs.getAttribute("igraDana");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 4) {
            zavrsiIgru();
        }
        //FacesContext.getCurrentInstance().responseComplete();

        entiteti.Asocijacije a;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(entiteti.Asocijacije.class);
        a = (entiteti.Asocijacije) cr.add(Restrictions.eq("asocID", igraDana.getAsocID())).uniqueResult();
        session.getTransaction().commit();
        session.close();

        asocijacije = a.getAsoc();
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/Projekat/faces/ucesnik.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Asocijacije.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void kreni() {
        pokrenut = true;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                ok[i][j] = false;
            }
        }
        for (int i = 0; i < 5; i++) {
            okResenja[i] = false;
        }
    }

    public void selektuj(int index1, int index2) {
        otvorenaPolja[index1][index2] = asocijacije.kolone[index1][index2];
        boja[index1][index2] = "plavaBoja";
        ok[index1][index2] = true;
    }

    public void gadjaj(int index) {
        if (index < 4) {
            if (resenja[index].equals(asocijacije.resenjaKolona[index])) {
                bojeResenja[index] = "zelenaBoja";
                okResenja[index] = true;
                brojPogodaka++;
                for (int i = 0; i < 4; i++) {
                    boja[index][i] = "zelenaBoja";
                    ok[index][i] = true;
                    otvorenaPolja[index][i] = asocijacije.kolone[index][i];
                }
            }
        } else {
            if (asocijacije.konacno.contains(resenja[index])) {
                bojeResenja[index] = "zelenaBoja";
                for (int i = 0; i < 5; i++) {
                    okResenja[i] = true;
                }
                brojPogodaka += 2;
                for (int i = 0; i < 4; i++) {
                    if (!bojeResenja[i].equals("zelenaBoja")) {
                        brojPogodaka++;
                    }
                    bojeResenja[i] = "zelenaBoja";
                    for (int j = 0; j < 4; j++) {
                        boja[i][j] = "zelenaBoja";
                        ok[i][j] = true;
                        otvorenaPolja[i][j] = asocijacije.kolone[i][j];
                    }
                }
                if (!bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void smanjiVreme() {
        if (pokrenut) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void boduj() {
        bodovanje = true;
        if (daLiJeSveURedu()) {
            poeni = brojPogodaka * 5;
            rezultatiIgreDana.setPoeni(rezultatiIgreDana.getPoeni() + poeni);

            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(rezultatiIgreDana);

            session.getTransaction().commit();
            session.close();
        }
    }

    public boolean daLiJeSveURedu() {
        //toDo
        return true;
    }
}
