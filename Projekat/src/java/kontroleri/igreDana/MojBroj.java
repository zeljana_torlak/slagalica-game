package kontroleri.igreDana;

import db.HibernateUtil;
import entiteti.IgraDana;
import entiteti.Korisnik;
import entiteti.RezultatiIgreDana;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@ManagedBean
@SessionScoped
@ViewScoped
public class MojBroj {

    String korIme;
    Korisnik kor;
    RezultatiIgreDana rezultatiIgreDana;
    IgraDana igraDana;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;

    int sledeciBroj;
    boolean[] ok;
    String[] brojevi;
    int sledecaPozicija;
    int[] pozicije;
    String resenje;
    int rezultat;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public RezultatiIgreDana getRezultatiIgreDana() {
        return rezultatiIgreDana;
    }

    public void setRezultatiIgreDana(RezultatiIgreDana rezultatiIgreDana) {
        this.rezultatiIgreDana = rezultatiIgreDana;
    }

    public IgraDana getIgraDana() {
        return igraDana;
    }

    public void setIgraDana(IgraDana igraDana) {
        this.igraDana = igraDana;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public int getSledeciBroj() {
        return sledeciBroj;
    }

    public void setSledeciBroj(int sledeciBroj) {
        this.sledeciBroj = sledeciBroj;
    }

    public boolean[] getOk() {
        return ok;
    }

    public void setOk(boolean[] ok) {
        this.ok = ok;
    }

    public String[] getBrojevi() {
        return brojevi;
    }

    public void setBrojevi(String[] brojevi) {
        this.brojevi = brojevi;
    }

    public int getSledecaPozicija() {
        return sledecaPozicija;
    }

    public void setSledecaPozicija(int sledecaPozicija) {
        this.sledecaPozicija = sledecaPozicija;
    }

    public int[] getPozicije() {
        return pozicije;
    }

    public void setPozicije(int[] pozicije) {
        this.pozicije = pozicije;
    }

    public String getResenje() {
        return resenje;
    }

    public void setResenje(String resenje) {
        this.resenje = resenje;
    }

    public int getRezultat() {
        return rezultat;
    }

    public void setRezultat(int rezultat) {
        this.rezultat = rezultat;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public MojBroj() {
        ok = new boolean[15];
        for (int i = 0; i < 15; i++) {
            ok[i] = true;
        }
        brojevi = new String[15];
        for (int i = 0; i < 9; i++) {
            brojevi[i] = "?";
        }
        brojevi[9] = "(";
        brojevi[10] = ")";
        brojevi[11] = "+";
        brojevi[12] = "-";
        brojevi[13] = "*";
        brojevi[14] = "/";
        preostaloVreme = 60;
        sledecaPozicija = poeni = rezultat = 0;
        pozicije = new int[30];
        resenje = "";
        bodovanje = false;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        rezultatiIgreDana = (RezultatiIgreDana) hs.getAttribute("rezultatiIgreDana");
        igraDana = (IgraDana) hs.getAttribute("igraDana");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 1) {
            sledecaIgra();
        } else {
            stanje++;
        }
        //FacesContext.getCurrentInstance().responseComplete();
    }

    public void sledecaIgra() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("rezultatiIgreDana", rezultatiIgreDana);
        hs.setAttribute("igraDana", igraDana);
        hs.setAttribute("stanje", stanje);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("skocko.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MojBroj.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void zavrsiMojBroj() {
        if (!bodovanje) {
            boduj();
        }
        sledecaIgra();
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/Projekat/faces/ucesnik.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MojBroj.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void stopBroj() {
        if (sledeciBroj < 3) {
            brojevi[sledeciBroj] = (igraDana.getMojBroj().mojBroj + "").substring(sledeciBroj, sledeciBroj + 1);
        } else {
            brojevi[sledeciBroj] = igraDana.getMojBroj().brojevi[sledeciBroj - 3] + "";
            ok[sledeciBroj] = false;
        }
        sledeciBroj++;
        if (sledeciBroj == 9) {
            ok[9] = ok[10] = ok[11] = ok[12] = ok[13] = ok[14] = false;
        }
    }

    public void dodaj(int indexBroj) {
        resenje = resenje + brojevi[indexBroj];
        pozicije[sledecaPozicija] = indexBroj;
        if (indexBroj < 9) {
            ok[indexBroj] = true;
        }
        sledecaPozicija++;
    }

    public void brisi() {
        sledecaPozicija--;
        int indexBroj = pozicije[sledecaPozicija];
        ok[indexBroj] = false;
        resenje = resenje.substring(0, resenje.length() - brojevi[indexBroj].length());
    }

    public void smanjiVreme() {
        if (sledeciBroj > 8) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void boduj() {
        bodovanje = true;
        if (daLiJeSveURedu()) {
            if (rezultat == igraDana.getMojBroj().mojBroj) {
                poeni = 10;
            } else if (Math.abs(rezultat - igraDana.getMojBroj().mojBroj) <= 10) {
                poeni = 5;
            }
            rezultatiIgreDana.setPoeni(rezultatiIgreDana.getPoeni() + poeni);

            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(rezultatiIgreDana);

            session.getTransaction().commit();
            session.close();
        }
    }

    public boolean daLiJeSveURedu() {
        try {
            rezultat = (int) new ScriptEngineManager().getEngineByName("JavaScript").eval(resenje);
            return true;
        } catch (ScriptException ex) {
            rezultat = 1 / 0;
            Logger.getLogger(MojBroj.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
