package kontroleri.igreDana;

import db.HibernateUtil;
import entiteti.IgraDana;
import entiteti.Korisnik;
import entiteti.RezultatiIgreDana;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Slagalica {

    String korIme;
    Korisnik kor;
    RezultatiIgreDana rezultatiIgreDana;
    IgraDana igraDana;
    String rec;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;

    int sledeceSlovo;
    boolean[] ok;
    String[] slova;
    int sledecaPozicija;
    int[] pozicije;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public RezultatiIgreDana getRezultatiIgreDana() {
        return rezultatiIgreDana;
    }

    public void setRezultatiIgreDana(RezultatiIgreDana rezultatiIgreDana) {
        this.rezultatiIgreDana = rezultatiIgreDana;
    }

    public IgraDana getIgraDana() {
        return igraDana;
    }

    public void setIgraDana(IgraDana igraDana) {
        this.igraDana = igraDana;
    }

    public int getSledeceSlovo() {
        return sledeceSlovo;
    }

    public void setSledeceSlovo(int sledeceSlovo) {
        this.sledeceSlovo = sledeceSlovo;
    }

    public boolean[] getOk() {
        return ok;
    }

    public void setOk(boolean[] ok) {
        this.ok = ok;
    }

    public String[] getSlova() {
        return slova;
    }

    public void setSlova(String[] slova) {
        this.slova = slova;
    }

    public String getRec() {
        return rec;
    }

    public void setRec(String rec) {
        this.rec = rec;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public int getSledecaPozicija() {
        return sledecaPozicija;
    }

    public void setSledecaPozicija(int sledecaPozicija) {
        this.sledecaPozicija = sledecaPozicija;
    }

    public int[] getPozicije() {
        return pozicije;
    }

    public void setPozicije(int[] pozicije) {
        this.pozicije = pozicije;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public Slagalica() {
        ok = new boolean[12];
        for (int i = 0; i < 12; i++) {
            ok[i] = true;
        }
        slova = new String[12];
        for (int i = 0; i < 12; i++) {
            slova[i] = "?";
        }
        rec = "";
        preostaloVreme = 60;
        sledecaPozicija = poeni = 0;
        pozicije = new int[12];
        bodovanje = false;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        rezultatiIgreDana = (RezultatiIgreDana) hs.getAttribute("rezultatiIgreDana");
        igraDana = (IgraDana) hs.getAttribute("igraDana");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 0) {
            sledecaIgra();
        } else {
            stanje++;
        }
        //FacesContext.getCurrentInstance().responseComplete();
    }

    public void sledecaIgra() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("rezultatiIgreDana", rezultatiIgreDana);
        hs.setAttribute("igraDana", igraDana);
        hs.setAttribute("stanje", stanje);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("mojBroj.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Slagalica.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void zavrsiSlagalicu() {
        if (!bodovanje) {
            boduj();
        }
        sledecaIgra();
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/Projekat/faces/ucesnik.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Slagalica.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void stopSlova() {
        slova[sledeceSlovo] = igraDana.getSlagalica().slova[sledeceSlovo];
        ok[sledeceSlovo] = false;
        sledeceSlovo++;
    }

    public void dodaj(int indexSlova) {
        rec = rec + slova[indexSlova];
        pozicije[sledecaPozicija] = indexSlova;
        ok[indexSlova] = true;
        sledecaPozicija++;
    }

    public void brisi() {
        sledecaPozicija--;
        int indexSlova = pozicije[sledecaPozicija];
        ok[indexSlova] = false;
        rec = rec.substring(0, rec.length() - slova[indexSlova].length());
    }

    public void smanjiVreme() {
        if (sledeceSlovo > 11) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void boduj() {
        bodovanje = true;
        if (daLiJeSveURedu()) {
            poeni = 2 * sledecaPozicija;
            rezultatiIgreDana.setPoeni(rezultatiIgreDana.getPoeni() + poeni);

            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(rezultatiIgreDana);

            session.getTransaction().commit();
            session.close();
        }
    }

    public boolean daLiJeSveURedu() {
        entiteti.Slagalica k;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(entiteti.Slagalica.class);
        k = (entiteti.Slagalica) cr.add(Restrictions.eq("rec", rec)).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return k != null;
    }
}
