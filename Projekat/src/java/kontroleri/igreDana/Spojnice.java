package kontroleri.igreDana;

import db.HibernateUtil;
import entiteti.IgraDana;
import entiteti.Korisnik;
import entiteti.RezultatiIgreDana;
import helper.SPKlasa;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
@ViewScoped
public class Spojnice {

    String korIme;
    Korisnik kor;
    RezultatiIgreDana rezultatiIgreDana;
    IgraDana igraDana;
    int preostaloVreme;
    int poeni;
    boolean bodovanje;
    int stanje;

    SPKlasa spojnice;
    String[] drugaKolona;
    int sledecaPozicija;
    boolean pokrenut;
    int brojPogodaka;
    boolean[] ok;
    String[][] boja;

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public Korisnik getKor() {
        return kor;
    }

    public void setKor(Korisnik kor) {
        this.kor = kor;
    }

    public RezultatiIgreDana getRezultatiIgreDana() {
        return rezultatiIgreDana;
    }

    public void setRezultatiIgreDana(RezultatiIgreDana rezultatiIgreDana) {
        this.rezultatiIgreDana = rezultatiIgreDana;
    }

    public IgraDana getIgraDana() {
        return igraDana;
    }

    public void setIgraDana(IgraDana igraDana) {
        this.igraDana = igraDana;
    }

    public int getPreostaloVreme() {
        return preostaloVreme;
    }

    public void setPreostaloVreme(int preostaloVreme) {
        this.preostaloVreme = preostaloVreme;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public boolean isBodovanje() {
        return bodovanje;
    }

    public void setBodovanje(boolean bodovanje) {
        this.bodovanje = bodovanje;
    }

    public String[] getDrugaKolona() {
        return drugaKolona;
    }

    public void setDrugaKolona(String[] drugaKolona) {
        this.drugaKolona = drugaKolona;
    }

    public int getSledecaPozicija() {
        return sledecaPozicija;
    }

    public void setSledecaPozicija(int sledecaPozicija) {
        this.sledecaPozicija = sledecaPozicija;
    }

    public boolean isPokrenut() {
        return pokrenut;
    }

    public void setPokrenut(boolean pokrenut) {
        this.pokrenut = pokrenut;
    }

    public int getBrojPogodaka() {
        return brojPogodaka;
    }

    public void setBrojPogodaka(int brojPogodaka) {
        this.brojPogodaka = brojPogodaka;
    }

    public SPKlasa getSpojnice() {
        return spojnice;
    }

    public void setSpojnice(SPKlasa spojnice) {
        this.spojnice = spojnice;
    }

    public boolean[] getOk() {
        return ok;
    }

    public void setOk(boolean[] ok) {
        this.ok = ok;
    }

    public String[][] getBoja() {
        return boja;
    }

    public void setBoja(String[][] boja) {
        this.boja = boja;
    }

    public Spojnice() {
        drugaKolona = new String[10];
        for (int i = 0; i < 10; i++) {
            drugaKolona[i] = "?";
        }
        preostaloVreme = 60;
        sledecaPozicija = poeni = 0;
        bodovanje = false;
        pokrenut = false;
        brojPogodaka = 0;
        ok = new boolean[10];
        for (int i = 0; i < 10; i++) {
            ok[i] = true;
        }
        boja = new String[2][];
        for (int i = 0; i < 2; i++) {
            boja[i] = new String[10];
            for (int j = 0; j < 10; j++) {
                boja[i][j] = "belaBoja";
            }
        }

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        kor = (Korisnik) hs.getAttribute("korisnik");
        korIme = kor.getKorIme();
        rezultatiIgreDana = (RezultatiIgreDana) hs.getAttribute("rezultatiIgreDana");
        igraDana = (IgraDana) hs.getAttribute("igraDana");
        stanje = (int) hs.getAttribute("stanje");
        if (stanje != 3) {
            sledecaIgra();
        } else {
            stanje++;
        }
        //FacesContext.getCurrentInstance().responseComplete();

        entiteti.Spojnice s;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(entiteti.Spojnice.class);
        s = (entiteti.Spojnice) cr.add(Restrictions.eq("spojID", igraDana.getSpojID())).uniqueResult();
        session.getTransaction().commit();
        session.close();

        spojnice = s.getSpoj();
    }

    public void sledecaIgra() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        hs.setAttribute("rezultatiIgreDana", rezultatiIgreDana);
        hs.setAttribute("igraDana", igraDana);
        hs.setAttribute("stanje", stanje);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("asocijacije.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Spojnice.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void zavrsiSpojnice() {
        if (!bodovanje) {
            boduj();
        }
        sledecaIgra();
    }

    public void zavrsiIgru() {
        if (!bodovanje) {
            boduj();
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        hs.setAttribute("korisnik", kor);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/Projekat/faces/ucesnik.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Spojnice.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void kreni() {
        Random rand = new Random();
        LinkedList<Integer> lista = new LinkedList<>();
        int r = rand.nextInt(10);

        for (int i = 0; i < 10; i++) {
            while (lista.contains(r)) {
                r = rand.nextInt(10);
            }
            lista.add(r);
            drugaKolona[i] = spojnice.kolona2[r];
        }

        pokrenut = true;
        for (int i = 0; i < 10; i++) {
            ok[i] = false;
        }

        boja[0][sledecaPozicija] = "plavaBoja";
    }

    public void selektuj(int index) {
        if (drugaKolona[index].equals(spojnice.kolona2[sledecaPozicija])) {
            boja[0][sledecaPozicija] = "zelenaBoja";
            boja[1][index] = "zelenaBoja";
            ok[index] = true;
            brojPogodaka++;
        } else {
            boja[0][sledecaPozicija] = "crvenaBoja";
        }
        sledecaPozicija++;
        if (sledecaPozicija == 10) {
            for (int i = 0; i < 10; i++) {
                ok[i] = true;
            }
            if (!bodovanje) {
                boduj();
            }
        } else {
            boja[0][sledecaPozicija] = "plavaBoja";
        }
    }

    public void smanjiVreme() {
        if (pokrenut) {
            if (preostaloVreme > 0) {
                preostaloVreme--;
                if (preostaloVreme == 0 && !bodovanje) {
                    boduj();
                }
            }
        }
    }

    public void boduj() {
        bodovanje = true;
        if (daLiJeSveURedu()) {
            poeni = brojPogodaka;
            rezultatiIgreDana.setPoeni(rezultatiIgreDana.getPoeni() + poeni);

            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session session = factory.openSession();
            session.beginTransaction();

            session.update(rezultatiIgreDana);

            session.getTransaction().commit();
            session.close();
        }
    }

    public boolean daLiJeSveURedu() {
        //toDo
        return true;
    }
}
