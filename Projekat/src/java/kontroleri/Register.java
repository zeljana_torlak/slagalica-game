package kontroleri;

import db.HibernateUtil;
import entiteti.Korisnik;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.UploadedFile;
import org.primefaces.shaded.commons.io.IOUtils;

@ManagedBean
@RequestScoped
public class Register {

    String ime;
    String prezime;
    String mejl;
    String zanimanje;
    String korIme;
    String lozinka;
    String lozinka2;
    int pol;
    Date datRodjenja;
    byte[] slika;
    UploadedFile f;

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getMejl() {
        return mejl;
    }

    public void setMejl(String mejl) {
        this.mejl = mejl;
    }

    public String getZanimanje() {
        return zanimanje;
    }

    public void setZanimanje(String zanimanje) {
        this.zanimanje = zanimanje;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getLozinka2() {
        return lozinka2;
    }

    public void setLozinka2(String lozinka2) {
        this.lozinka2 = lozinka2;
    }

    public int getPol() {
        return pol;
    }

    public void setPol(int pol) {
        this.pol = pol;
    }

    public Date getDatRodjenja() {
        return datRodjenja;
    }

    public void setDatRodjenja(Date datRodjenja) {
        this.datRodjenja = datRodjenja;
    }

    public byte[] getSlika() {
        return slika;
    }

    public void setSlika(byte[] slika) {
        this.slika = slika;
    }

    public UploadedFile getF() {
        return f;
    }

    public void setF(UploadedFile f) {
        this.f = f;
    }

    public void register() {
        if (lozinka == null ? lozinka2 != null : !lozinka.equals(lozinka2)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Unete lozinke se ne poklapaju!"));
            return;
        }

        if (f == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Nije izabrana slika!"));
            return;
        }

        try {
            slika = IOUtils.toByteArray(f.getInputstream());
        } catch (IOException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(slika));
            if (img.getHeight() > 300 || img.getWidth() > 300) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Izabrana slika je prevelika (podržano je najviše 300x300px)!"));
                return;
            }
        } catch (IOException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!proveri()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Korisnik sa izabranim korisničkim imenom već postoji u sistemu!"));
            return;
        }

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Korisnik korisnik = new Korisnik();
        korisnik.setIme(ime);
        korisnik.setPrezime(prezime);
        korisnik.setMejl(mejl);
        korisnik.setZanimanje(zanimanje);
        korisnik.setKorIme(korIme);
        try {
            korisnik.setLozinka(HashSHA2(lozinka));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }
        korisnik.setPol(pol);
        korisnik.setDatRodjenja(datRodjenja);

        Blob b;
        b = (Blob) Hibernate.getLobCreator(session).createBlob(slika);
        korisnik.setSlika(b);
        korisnik.setTip(4);

        session.save(korisnik);

        session.getTransaction().commit();
        session.close();

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Uspešno ste poslali zahtev za registraciju!"));
    }

    public String HashSHA2(String value) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(value.getBytes("UTF-8"));

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < encodedhash.length; i++) {
            builder.append(Integer.toString((encodedhash[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return builder.toString();
    }

    private boolean proveri() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Criteria cr = session.createCriteria(Korisnik.class);
        Korisnik korisnik;
        korisnik = (Korisnik) cr.add(Restrictions.eq("korIme", korIme)).uniqueResult();
        session.getTransaction().commit();
        session.close();

        return korisnik == null;
    }

}
