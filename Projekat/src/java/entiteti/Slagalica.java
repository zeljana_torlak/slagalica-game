package entiteti;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Slagalica implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "slagID")
    int slagID;

    @Column(name = "rec")
    String rec;

    public int getSlagID() {
        return slagID;
    }

    public void setSlagID(int slagID) {
        this.slagID = slagID;
    }

    public String getRec() {
        return rec;
    }

    public void setRec(String rec) {
        this.rec = rec;
    }

}
