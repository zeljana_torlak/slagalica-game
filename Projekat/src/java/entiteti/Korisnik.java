package entiteti;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Korisnik implements Serializable {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "korID")
    String korID;

    @Id
    @Column(name = "korIme")
    String korIme;

    @Column(name = "ime")
    String ime;

    @Column(name = "prezime")
    String prezime;

    @Column(name = "mejl")
    String mejl;

    @Column(name = "zanimanje")
    String zanimanje;

    @Column(name = "lozinka")
    String lozinka;

    @Column(name = "pol")
    int pol;

    @Column(name = "datRodjenja")
    Date datRodjenja;

    @Lob
    @Column(name = "slika")
    Blob slika;

    @Column(name = "tip")
    int tip;

    public String getKorID() {
        return korID;
    }

    public void setKorID(String korID) {
        this.korID = korID;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getMejl() {
        return mejl;
    }

    public void setMejl(String mejl) {
        this.mejl = mejl;
    }

    public String getZanimanje() {
        return zanimanje;
    }

    public void setZanimanje(String zanimanje) {
        this.zanimanje = zanimanje;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public int getPol() {
        return pol;
    }

    public void setPol(int pol) {
        this.pol = pol;
    }

    public Date getDatRodjenja() {
        return datRodjenja;
    }

    public void setDatRodjenja(Date datRodjenja) {
        this.datRodjenja = datRodjenja;
    }

    public Blob getSlika() {
        return slika;
    }

    public void setSlika(Blob slika) {
        this.slika = slika;
    }

    public int getTip() {
        return tip;
    }

    public void setTip(int tip) {
        this.tip = tip;
    }

    public String imePola() {
        switch (pol) {
            case 1:
                return "muški";
            case 2:
                return "ženski";
            default:
                return "neutralni";
        }
    }

}
