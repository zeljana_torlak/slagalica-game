package entiteti;

import helper.AKlasa;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Asocijacije implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "asocID")
    int asocID;

    @Lob
    @Column(name = "asoc")
    AKlasa asoc;

    public int getAsocID() {
        return asocID;
    }

    public void setAsocID(int asocID) {
        this.asocID = asocID;
    }

    public AKlasa getAsoc() {
        return asoc;
    }

    public void setAsoc(AKlasa asoc) {
        this.asoc = asoc;
    }

}
