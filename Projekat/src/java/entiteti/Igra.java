package entiteti;

import helper.Partije;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Igra implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "igraID")
    int igraID;

    @Column(name = "korImePlavi")
    String korImePlavi;

    @Column(name = "korImeCrveni")
    String korImeCrveni;

    @Column(name = "poeniPlavog")
    int poeniPlavog;

    @Column(name = "poeniCrvenog")
    int poeniCrvenog;

    @Column(name = "vreme")
    Date vreme;

    @Column(name = "ishod")
    int ishod;

    @Column(name = "tip")
    int tip;

    @Lob
    @Column(name = "poeniPoPartijama")
    Partije poeniPoPartijama;

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public String getKorImePlavi() {
        return korImePlavi;
    }

    public void setKorImePlavi(String korImePlavi) {
        this.korImePlavi = korImePlavi;
    }

    public String getKorImeCrveni() {
        return korImeCrveni;
    }

    public void setKorImeCrveni(String korImeCrveni) {
        this.korImeCrveni = korImeCrveni;
    }

    public int getPoeniPlavog() {
        return poeniPlavog;
    }

    public void setPoeniPlavog(int poeniPlavog) {
        this.poeniPlavog = poeniPlavog;
    }

    public int getPoeniCrvenog() {
        return poeniCrvenog;
    }

    public void setPoeniCrvenog(int poeniCrvenog) {
        this.poeniCrvenog = poeniCrvenog;
    }

    public Date getVreme() {
        return vreme;
    }

    public void setVreme(Date vreme) {
        this.vreme = vreme;
    }

    public int getIshod() {
        return ishod;
    }

    public void setIshod(int ishod) {
        this.ishod = ishod;
    }

    public int getTip() {
        return tip;
    }

    public void setTip(int tip) {
        this.tip = tip;
    }

    public Partije getPoeniPoPartijama() {
        return poeniPoPartijama;
    }

    public void setPoeniPoPartijama(Partije poeniPoPartijama) {
        this.poeniPoPartijama = poeniPoPartijama;
    }

}
