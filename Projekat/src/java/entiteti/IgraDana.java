package entiteti;

import helper.MBKlasa;
import helper.SKKlasa;
import helper.SLKlasa;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class IgraDana implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "igraID")
    int igraID;

    @Lob
    @Column(name = "slagalica")
    SLKlasa slagalica;

    @Lob
    @Column(name = "mojBroj")
    MBKlasa mojBroj;

    @Lob
    @Column(name = "skocko")
    SKKlasa skocko;

    @Column(name = "spojID")
    int spojID;

    @Column(name = "asocID")
    int asocID;
    
    @Column(name = "datum")
    Date datum;

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public SLKlasa getSlagalica() {
        return slagalica;
    }

    public void setSlagalica(SLKlasa slagalica) {
        this.slagalica = slagalica;
    }

    public MBKlasa getMojBroj() {
        return mojBroj;
    }

    public void setMojBroj(MBKlasa mojBroj) {
        this.mojBroj = mojBroj;
    }

    public SKKlasa getSkocko() {
        return skocko;
    }

    public void setSkocko(SKKlasa skocko) {
        this.skocko = skocko;
    }

    public int getSpojID() {
        return spojID;
    }

    public void setSpojID(int spojID) {
        this.spojID = spojID;
    }

    public int getAsocID() {
        return asocID;
    }

    public void setAsocID(int asocID) {
        this.asocID = asocID;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

}
