package entiteti;

import helper.Sinc;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Sinhronizacija implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "igraID")
    int igraID;

    @Lob
    @Column(name = "sinc")
    Sinc sinc;

    public int getIgraID() {
        return igraID;
    }

    public void setIgraID(int igraID) {
        this.igraID = igraID;
    }

    public Sinc getSinc() {
        return sinc;
    }

    public void setSinc(Sinc sinc) {
        this.sinc = sinc;
    }

}
