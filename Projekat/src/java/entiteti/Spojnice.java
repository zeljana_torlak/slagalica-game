package entiteti;

import helper.SPKlasa;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Spojnice implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "spojID")
    int spojID;

    @Lob
    @Column(name = "spoj")
    SPKlasa spoj;

    public int getSpojID() {
        return spojID;
    }

    public void setSpojID(int spojID) {
        this.spojID = spojID;
    }

    public SPKlasa getSpoj() {
        return spoj;
    }

    public void setSpoj(SPKlasa spoj) {
        this.spoj = spoj;
    }

}
