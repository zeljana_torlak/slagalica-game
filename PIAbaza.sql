SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

DROP DATABASE IF EXISTS `baza`;
CREATE DATABASE IF NOT EXISTS `baza` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `baza`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `korisnik`;
CREATE TABLE IF NOT EXISTS `korisnik` (
  `korID` INT(11) NOT NULL UNIQUE AUTO_INCREMENT,
  `korIme` VARCHAR(45) COLLATE utf8_unicode_ci NOT NULL UNIQUE,
  `ime` VARCHAR(45) COLLATE utf8_unicode_ci NOT NULL,
  `prezime` VARCHAR(45) COLLATE utf8_unicode_ci NOT NULL,
  `mejl` VARCHAR(45) COLLATE utf8_unicode_ci NOT NULL,
  `zanimanje` VARCHAR(45) COLLATE utf8_unicode_ci NOT NULL,
  `lozinka` VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL,
  `pol` INT(11) NOT NULL,
  `datRodjenja` DATE NOT NULL,
  `slika` BLOB NOT NULL,
  `tip` INT(11) NOT NULL COMMENT '1-ucesnik, 2-admin, 3-supervizor, 4-zahtev',
  PRIMARY KEY (`korIme`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `korisnik` (`korID`, `korIme`, `ime`, `prezime`, `mejl`, 
 `zanimanje`, `lozinka`, `pol`, `datRodjenja`, `slika`, `tip`) VALUES
(1, 'novica', 'Novica', 'Novičić', 'novin@gmail.com', 'novinar', SHA2('Novica123#',0), 1, '1990-1-25', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/zvezda.png'), 1),
(2, 'laza', 'Laza', 'Lazić', 'laza@gmail.com', 'genije', SHA2('Laza123#',0), 1, '1980-3-3', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/pik.png'), 2),
(3, 'milki', 'Milka', 'Canić', 'milki@gmail.com', 'dobro veče', SHA2('Milki123#',0), 2, '1960-1-1', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/skocko.png'), 3),
(4, 'ana', 'Ana', 'Banana', 'banana@gmail.com', 'banana', SHA2('Banana123#',0), 2, '2018-5-5', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/herc.png'), 1),
(5, 'jaca', 'Jagoda', 'Jaca', 'jagoda@gmail.com', 'jagoda', SHA2('Jagoda123#',0), 2, '2018-5-5', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/tref.png'), 1),
(6, 'ruska', 'Kruška', 'Ruška', 'kruska@gmail.com', 'kruska', SHA2('Kruska123#',0), 2, '2018-5-5', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/karo.png'), 1),
(7, 'kolacic', 'Slatki', 'Kolačić', 'kolacic@gmail.com', 'proizvod', SHA2('Kolacic123#',0), 1, '2018-12-12', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/smajli.png'), 1),
(8, 'kravica', 'Kravica', 'Mlečna', 'kravica@gmail.com', 'proizvođac', SHA2('Kravica123#',0), 2, '1995-12-12', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/cow.jpg'), 1),
(9, 'random', 'Slučajni', 'Prolaznik', 'slucajni@gmail.com', 'prolaznik', SHA2('Random123#',0), 1, '1999-2-2', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/znakovi.png'), 4),
(10, 'krem', 'Krem', 'Bananica', 'krem@gmail.com', 'slatkoća', SHA2('Krem123#',0), 2, '2019-6-6', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/cow.jpg'), 4),
(11, 'cn', 'Cat', 'Noir', 'catnoir@gmail.com', 'superhero', SHA2('Miraculous123#',0), 1, '2015-12-1', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/CN1.png'), 4),
(12, 'lb', 'Lady', 'Bug', 'ladybug@gmail.com', 'superhero', SHA2('Miraculous123#',0), 2, '2015-12-1', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/LB1.png'), 4),
(13, 'rr', 'Rena', 'Rouge', 'renarouge@gmail.com', 'superhero', SHA2('Miraculous123#',0), 2, '2015-12-1', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/RR1.png'), 4),
(14, 'cp', 'Cara', 'Pace', 'carapace@gmail.com', 'superhero', SHA2('Miraculous123#',0), 1, '2015-12-1', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/CP1.png'), 4),
(15, 'qb', 'Queen', 'Bee', 'queenbee@gmail.com', 'superhero', SHA2('Miraculous123#',0), 2, '2015-12-1', LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/QB1.png'), 4);

-- --------------------------------------------------------

DROP TABLE IF EXISTS `igra`;
CREATE TABLE IF NOT EXISTS `igra` (
  `igraID` INT(11) NOT NULL UNIQUE AUTO_INCREMENT,
  `korImePlavi` VARCHAR(45) COLLATE utf8_unicode_ci NOT NULL,
  `korImeCrveni` VARCHAR(45) COLLATE utf8_unicode_ci,
  `poeniPlavog` INT(11) NOT NULL DEFAULT 0,
  `poeniCrvenog` INT(11) NOT NULL DEFAULT 0,
  `vreme` DATETIME NOT NULL,
  `poeniPoPartijama` BLOB,
  `ishod` INT(11) COMMENT '1-pobeda plavog, 2-pobeda crvenog, 3-nereseno',
  `tip` INT(11) NOT NULL COMMENT '1-zapoceo plavi, 2-pridruzio se crveni, 3-zavrsena',
  PRIMARY KEY (`igraID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `rezultatiigredana`;
CREATE TABLE IF NOT EXISTS `rezultatiigredana` (
  `igraID` INT(11) NOT NULL UNIQUE AUTO_INCREMENT,
  `korIme` VARCHAR(45) COLLATE utf8_unicode_ci NOT NULL,
  `poeni` INT(11) NOT NULL DEFAULT 0,
  `vreme` DATETIME NOT NULL,
  PRIMARY KEY (`igraID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `slagalica`;
CREATE TABLE IF NOT EXISTS `slagalica` (
  `slagID` INT(20) NOT NULL UNIQUE AUTO_INCREMENT,
  `rec` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`slagID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `slagalica` (`rec`) VALUES
('mačka'),
('pas'),
('slon'),
('krava'),
('petao'),
('orao'),
('žirafa'),
('slavuj'),
('medved'),
('irvas'),
('bik'),
('jarac'),
('riba'),
('vo'),
('sova'),
('delfin'),
('ajkula'),
('kit'),
('zec'),
('majmun'),
('bubamara'),
('pčela'),
('osa'),
('lisica'),
('vuk'),
('kornjača'),
('puž'),
('jež'),
('gusenica'),
('leptir'),
('paun'),
('miš'),
('žaba'),
('jelen'),
('srna'),
('lane'),
('tigar'),
('lav'),
('konj'),
('magarac'),
('zmaj'),
('dinosaurus'),
('zmija'),
('leopard'),
('gepard'),
('puma'),
('prase'),
('ovca'),
('koza'),
('kokoška'),
('pile'),
('patka'),
('guska'),
('labud'),
('šimpanza'),
('gorila'),
('orangutan'),
('cvrčak'),
('galeb'),
('orao'),
('jastreb'),
('albatros'),
('ptica'),
('školjka'),
('meduza'),
('rak'),
('lignja'),
('veverica'),
('rakun'),
('tvor'),
('plankton'),
('nosorog'),
('noj'),
('emu'),
('flamingos'),
('aligator'),
('krokodil'),
('jazavac'),
('dabar'),
('krtica'),
('panda'),
('koala'),
('kivi'),
('kengur'),
('šakal'),
('papagaj'),
('antilopa'),
('zebra'),
('mrav'),
('bumbar'),
('lasta'),
('hrt'),
('kunić'),
('roda'),
('čaplja'),
('detlić'),
('vrabac'),
('prepelica'),
('anakonda'),
('kobra'),
('gušter'),
('ara'),
('foka'),
('šaran'),
('som'),
('prisna'),
('prisni'),
('prisnima'),
('mirisna'),
('miris'),
('isprani'),
('isprana'),
('pasirana'),
('pasirani'),
('ispirana'),
('sramna'),
('sramni'),
('sirni'),
('pesma'),
('primesa'),
('maseri'),
('spremni'),
('spremna'),
('nar'),
('rana'),
('rasa'),
('sir'),
('ram'),
('mir'),
('masa'),
('mana'),
('mapa'),
('pare'),
('nemar'),
('mirna'),
('era'),
('as'),
('par'),
('ris'),
('pin'),
('mina'),
('sin'),
('sipana'),
('pisani'),
('mapirani'),
('pisarima'),
('masirana'),
('pisana'),
('sipani');

-- --------------------------------------------------------

DROP TABLE IF EXISTS `spojnice`;
CREATE TABLE IF NOT EXISTS `spojnice` (
  `spojID` INT(20) NOT NULL UNIQUE AUTO_INCREMENT,
  `spoj` BLOB NOT NULL,
  PRIMARY KEY (`spojID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `spojnice` (`spojID`, `spoj`) VALUES
 (1, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj1.txt')),
 (2, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj2.txt')),
 (3, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj3.txt')),
 (4, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj4.txt')),
 (5, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj5.txt')),
 (6, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj6.txt')),
 (7, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj7.txt')),
 (8, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj8.txt')),
 (9, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj9.txt')),
 (10, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/spoj10.txt'));

-- --------------------------------------------------------

DROP TABLE IF EXISTS `asocijacije`;
CREATE TABLE IF NOT EXISTS `asocijacije` (
  `asocID` INT(20) NOT NULL UNIQUE AUTO_INCREMENT,
  `asoc` BLOB NOT NULL,
  PRIMARY KEY (`asocID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `asocijacije` (`asocID`, `asoc`) VALUES
 (1, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc1.txt')),
 (2, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc2.txt')),
 (3, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc3.txt')),
 (4, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc4.txt')),
 (5, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc5.txt')),
 (6, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc6.txt')),
 (7, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc7.txt')),
 (8, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc8.txt')),
 (9, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc9.txt')),
 (10, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/asoc10.txt'));

-- --------------------------------------------------------

DROP TABLE IF EXISTS `igradana`;
CREATE TABLE IF NOT EXISTS `igradana` (
  `igraID` INT(20) NOT NULL UNIQUE AUTO_INCREMENT,
  `slagalica` BLOB NOT NULL,
  `mojBroj` BLOB NOT NULL,
  `skocko` BLOB NOT NULL,
  `spojID` INT(20) NOT NULL,
  `asocID` INT(20) NOT NULL,
  `datum` DATE NOT NULL,
  PRIMARY KEY (`igraID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `igradana` (`igraID`, `slagalica`, `mojBroj`, `skocko`, `spojID`, `asocID`, `datum`) VALUES
 (1, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/slag1.txt'), LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/mbr1.txt'), LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/skoc1.txt'), 8, 6, '2019-7-12'),
 (2, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/slag2.txt'), LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/mbr2.txt'), LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/skoc2.txt'), 1, 9, '2019-7-11'),
 (3, LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/slag3.txt'), LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/mbr3.txt'), LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/skoc3.txt'), 4, 2, '2019-7-14');

-- --------------------------------------------------------

DROP TABLE IF EXISTS `sinhronizacija`;
CREATE TABLE IF NOT EXISTS `sinhronizacija` (
  `igraID` INT(11) NOT NULL UNIQUE AUTO_INCREMENT,
  `sinc` BLOB,
  PRIMARY KEY (`igraID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------


--
-- Constraints for dumped tables
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;